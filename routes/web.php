<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix'=>'admin','middleware'=>['can:admin']],function (){
    Route::group(['prefix'=>'user'],function (){

        Route::get('them-moi','UserController@addUser')->name('AddUserBackend');

        Route::get('danh-sach','UserController@listUser')->name('ListUser');
        Route::post('delete-user/{id?}','UserController@delUser')->name('delUser');

        Route::get('edit/{id?}','UserController@editUser')->name('UserBackend');
        Route::post('edit/{id?}','UserController@postEditUser')->name('PostUserBackend');

    });
    Route::group(['prefix'=>'category'],function (){

        Route::get('them-moi/','CategoryController@addCate')->name('AddCate');

        Route::get('danh-sach','CategoryController@listCate')->name('ListCate');
        Route::post('delete-cate/{id?}','CategoryController@delCate')->name('delCate');

        Route::get('edit/{id?}','CategoryController@editCate')->name('AddEditCate');
        Route::post('edit/{id?}','CategoryController@postAddOrEditCate')->name('postAddOrEditCate');
    });
    Route::group(['prefix'=>'listcategory'],function () {
        Route::get('them-moi','ListCateController@addListCate')->name('AddListCate');

        Route::get('danh-sach', 'ListCateController@listCate')->name('List_Cate');
        Route::post('delete-list-cate/{id?}', 'ListCateController@delListCate')->name('delListCate');

        Route::get('edit/{id?}',  'ListCateController@editListCate')->name('AddEditListCate');
        Route::post('add-edit/{id?}', 'ListCateController@postAddOrEditListCate')->name('PostAddEditListCate');
    });

    Route::group(['prefix'=>'order'],function () {
        Route::get('list-order', 'OrderController@listOder')->name('listOder');
        Route::get('detail-order/{id}','OrderController@detailOrder')->name('detailOrder');
        Route::get('confirm-order/{id}','OrderController@confirmOrder')->name('ConfirmOrder');
    });
    Route::group(['prefix'=>'product'],function (){
        Route::get('them-moi/','ProductController@AddOrEditProduct')->name('AddProduct');

        Route::get('export','ProductController@export')->name('export');

        Route::get('/ajax-select-list-cate/{id?}','ProductController@ajaxProduct');

        Route::get('/ajax-select-eddit-listacte/{id?}','ProductController@ajaxSelectedEditListacte');

        Route::get('list-product','ProductController@listProduct')->name('list-product');

        Route::post('delete-product/{id?}','ProductController@deleteProduct')->name('deleteProduct');


        Route::get('editproduct/{id?}','ProductController@AddOrEditProduct')->name('editProduct');
        Route::post('editproduct/{id?}','ProductController@postAddOrEditProduct')->name('postAddOrEditProduct');
    });
    Route::group(['prefix'=>'slide'],function () {
        Route::get('them-moi/','SlideController@addOrEditSlide')->name('AddSlide');

        Route::get('danh-sach','SlideController@listSlide')->name('listSlide');
        Route::post('delete-slide','SlideController@postDelSlide')->name('postDelSlide');

        Route::get('edit/{id?}','SlideController@addOrEditSlide')->name('AddEditSlide');
        Route::post('edit/{id?}','SlideController@postAddOrditSlide')->name('postAddOrditSlide');
    });
    Route::group(['prefix'=>'statistical'],function () {
        Route::get('price-age-gender-order','StatisticalController@statisticalPrice')->name('statisticalPrice');
        Route::get('the-period','StatisticalController@statisticalThePeriod')->name('statisticalThePeriod');
        Route::post('search-date-the-period','StatisticalController@statisticalSearchDateThePeriod');
        Route::get('search-statistical-product','StatisticalController@StatisticalProduct')->name('statisticalProduct');

        Route::get('search-statistical-product-form','StatisticalController@StatisticalProductForm')->name('statisticalProductForm');
        Route::post('search-statistical-product-form','StatisticalController@PostStatisticalProductForm')->name('PostStatisticalProductForm');

        Route::get('search-statistical-product-view-form','StatisticalController@StatisticalProductViewForm')->name('statisticalProductViewForm');
        Route::get('search-statistical-product-price-form','StatisticalController@StatisticalProductPriceForm')->name('statisticalProductpriceForm');
        Route::post('search-statistical-product-view-form','StatisticalController@PostStatisticalProductViewForm')->name('PostStatisticalProductViewForm');

    });
});
Route::group(['prefix'=>'fontend'],function (){
    Route::get('login','LoginController@login')->name('login')->middleware('CheckBackLogin');
    Route::post('login','LoginController@postLogin')->name('postLogin');

    Route::get('add-user/','RegistrationController@AddOrEditUserFontend')->name('AddUserFontend');

    Route::get('edit-user/{id?}','RegistrationController@AddOrEditUserFontend')->name('AddEditUserFontend');
    Route::post('edit-user/{id?}','RegistrationController@PostAddOrEditUserFontend')->name('PostAddOrEditUserFontend');

    Route::get('logout','LoginController@logout')->name('logout');

    Route::get('list-product/{id}','PageController@listProduct')->name('listProduct');

    Route::get('details-product/{id}','PageController@DetailsPro')->name('DetailsPro');


    Route::post('comment','CommentController@Comnent')->name('Comment');
    Route::get('delete-comment/{id}','CommentController@deleteComment')->name('deleteComment');

    Route::get('add-cart','OrderController@AddCart')->name('AddCart');
    Route::get('show-order','OrderController@ShowOrder')->name('ShowOrder');

    Route::get('check-login','LoginController@checklogin')->name('checkLogin');
    Route::post('edit-qty/{id?}/{qty?}','OrderController@editqty')->name('editQty');
    Route::post('delete-product-checkout','OrderController@DeleteProCheckout')->name('DeleteProCheckout');
    Route::post('add-cart-detail','OrderController@ddCartDetail')->name('addCartDetail');

    Route::get('checkout','OrderController@checkout')->name('checkout')->middleware('CheckOrder');
    Route::post('payment','OrderController@payment')->name('payment')->middleware('CheckOrder');;

    Route::get('history/{id}','OrderController@history')->name('history');
    Route::post('delete-history/','OrderController@deleteHistory');
    Route::post('search','ProductController@search')->name('search');
    Route::post('comment','CommentController@Comment')->name('Comment');

    Route::get('all-new-product','OrderController@allNewProduct')->name('allNewProduct');
    Route::get('product-buys','OrderController@proBuys')->name('proBuys');
    Route::get('all-view','OrderController@allView')->name('all-View');

    Route::get('event','PageController@Event')->name('Event');
    Route::get('services','PageController@Services')->name('Services');
    Route::get('about','PageController@About')->name('About');

});

Route::get('/','ContentController@home')->name('home');

Route::get('tung',function (){
    $countOrder=\App\Models\OrderDetail::with('orderdetail_product')
        ->select('product_id',\Illuminate\Support\Facades\DB::raw('SUM(quanlity)as total_quanlity'))
        ->whereBetween('created_at',['2018-10-14' ,'2018-10-24'])
        ->groupBy('product_id')
        ->havingRaw('SUM(quanlity)')
        ->get();
    dd( $countOrder);
});