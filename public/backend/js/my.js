$(document).ready(function () {


    $('.oldDate').show();
    var end = 0;
    var start = 0;
    $('#datesale').change(function () {
        var date = $(this).val().split('-');
        var year = date[0];
        var month = date[1];
        var day = date[2];
        start = Number(year + month + day);
        var date = $('#tung').val().split('-');
        var year = date[0];
        var month = date[1];
        var day = date[2];
        end = Number(year + month + day);
        if (start > end && end != 0) {
            $('#customDate').show();
            $('#customDate').text('ngày bắt đầu phải nhỏ hơn ngày kết thúc');
            $('.custom4').attr('disabled', 'disabled');
        } else {

            $('#customDate').hide();
            $('.custom4').removeAttr('disabled');
        }

    });
    $('#thanh').change(function () {
        var date = $(this).val().split('-');
        var year = date[0];
        var month = date[1];
        var day = date[2];
        start = Number(year + month + day);
        if (start > end && end != 0) {
            $('#customDate').show();
            $('#customDate').text('ngày bắt đầu phải nhỏ hơn ngày kết thúc');
            // alert('ngày bắt đầu phải nhỏ hơn ngày kết thúc');
            $('.custom4').attr('disabled', 'disabled');
        } else {

            $('#customDate').hide();
            $('.custom4').removeAttr('disabled');
        }

    });
    $('#tung').change(function () {
        var date = $(this).val().split('-');
        var year = date[0];
        var month = date[1];
        var day = date[2];
        end = Number(year + month + day);
        if ($('#thanh').length) {
            date = $('#thanh').val().split('-');
        } else {
            date = $('#datesale').val().split('-');
        }
        var year = date[0];
        var month = date[1];
        var day = date[2];
        start = Number(year + month + day);
        if (start > end) {
            $('#customDate').show();
            $('#customDate').text('ngày bắt đầu phải nhỏ hơn ngày kết thúc');
            // alert('ngày bắt đầu phải nhỏ hơn ngày kết thúc');
            $('.custom4').attr('disabled', 'disabled');
        } else {

            $('#customDate').hide();
            $('.custom4').removeAttr('disabled');
        }

    });
    $('.start_at').hide();
    $('.end_at').hide();
    $('#start').show();
    $('#end').show();
    $('.dis_count').change(function () {
        $('.start_at').show();
        $('.end_at').show();

        if ($('.dis_count').val() == 0) {
            $('.custom4').removeAttr('disabled');
            $('.start_at').hide();
            $('.end_at').hide();
            $('.oldDate').hide();
        }
    });
    $('.oldDate1').change(function () {
        $('.oldDate').show();
        var end = 0;
        var start = 0;
        $('#thanh').change(function () {
            var date = $(this).val().split('-');
            var year = date[0];
            var month = date[1];
            var day = date[2];
            start = Number(year + month + day);
            $('#tung').change(function () {
                var date = $(this).val().split('-');
                var year = date[0];
                var month = date[1];
                var day = date[2];
                end = Number(year + month + day);
                if (start > end) {
                    $('#customDate').text('  ngày bắt đầu phải nhỏ hơn ngày kết thúc');
                    $('.custom4').attr('disabled', 'disabled')
                } else {
                    $('.custom4').removeAttr('disabled');
                }

            });
        });
        if ($('.oldDate1').val() == 0) {
            $('.custom4').removeAttr('disabled');
            $('.start_at').hide();
            $('.end_at').hide();
            $('.oldDate').hide();
        }
    });
    $('.hideTextDate').change(function () {
        var t = $(this).val();
        if (t == '') {
            $('.hideEdit').hide();

        } else {
            $('.hideEdit').show();
        }

    });

    $('#showpass').hide();
    $('#password').keyup(function () {
        $('#showpass').show();
        $('#showpass').mousedown(function () {
            $('#password').attr('type', 'text');
            $('#showpass').mouseup(function () {
                $('#password').attr('type', 'password');
            });
        });
        $('#password').mouseout(function () {
            $('#showpass').hide();
            $('#password').hover(function () {
                $('#showpass').show();
            });
        });
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.deluser').click(function () {
        var id = $(this).attr('attr');
        check = confirm('bạn có chắc chắn xóa không');
        if (check) {
            $.ajax
            ({
                'url': '/admin/user/delete-user/',
                'type': 'post',
                'data': {'id': id},
                success: function (data) {
                    if (data.errors == false) {
                        alert('bạn là admin, bạn không thể xóa được chính mình');
                    } else if (data.errors == true) {
                        alert('bạn không thể xóa do user đã từng comment hoặc từng mua sản phẩm');
                    } else {
                        alert('bạn đã xóa thành công');
                        location.reload();
                    }
                }

            });
        }

    });

    $('.dellCate').click(function () {
        var id = $(this).attr('attr');
        check = confirm('bạn có chắc chắn xóa không');
        if (check) {
            $.ajax
            ({
                'url': '/admin/category/delete-cate/',
                'type': 'post',
                'data': {'id': id},
                success: function (data) {
                    if (data.errors == false) {
                        alert('danh mục đã tồn tại trong danh mục con vui lòng xóa danh mục con trước khi xóa danh mục này');
                    } else if (data.errors == true) {
                        alert('bạn đã xóa thành công');
                        location.reload();

                    }
                }

            });
        }
    });

    $('.deListCate').click(function () {
        var id = $(this).attr('attr');
        check = confirm('bạn có chắc chắn xóa không');
        if (check) {
            $.ajax
            ({
                'url': '/admin/listcategory/delete-list-cate/',
                'type': 'post',
                'data': {'id': id},
                success: function (data) {
                    if (data.errors == false) {
                        alert('danh mục con này đã tồn tại bảng sản phẩm vui lòng xóa sản phẩm thuộc danh mục con trước khi xóa danh mục');
                    } else if (data.errors == true) {
                        alert('bạn đã xóa thành công');
                        location.reload();

                    }
                }

            });
        }
    });
    $('.cate').change(function () {
        var id = $(this).val();
        $.ajax
        ({
            'url': '/admin/product/ajax-select-list-cate',
            'type': 'get',
            'data': {'id': id},
            success: function (data) {
                $('.listacte').html(data);
            }

        });

    });
    var id = $('.cate').val();
    $.ajax
    ({
        'url': '/admin/product/ajax-select-list-cate',
        'type': 'get',
        'data': {'id': id},
        success: function (data) {
            $('.listacte').html(data);
        }

    });
    var id = $('.cate').val();
    var idpro = $('.cate').attr('attr');
    if (idpro != undefined) {
        $.ajax
        ({
            'url': '/admin/product/ajax-select-eddit-listacte',
            'type': 'get',
            'cache': 'false',
            'data': {'id': id, 'idpro': idpro},
            success: function (data) {
                $('.listacte').html(data);
            }

        });
    }

    $('.delpro').click(function () {
        var id = $(this).attr('attr');
        check = confirm('bạn có chắc chắn xóa không');
        if (check) {
            $.ajax
            ({
                'url': 'delete-product/',
                'type': 'post',
                'data': {'id': id},
                success: function (data) {
                    if (data.errors == false) {
                        alert('bạn không thể xóa do sản phẩm này đã từng mua hàng hoặc comment sản phẩm cả bạn');
                    } else if (data.errors == true) {
                        alert('bạn đã xóa thành công');
                        location.reload();

                    }
                }

            });
        }
    });
    $('.delSlide').click(function () {
        var id = $(this).attr('attr');
        check = confirm('bạn có chắc chắn xóa không');
        if (check) {
            $.ajax
            ({
                'url': '/admin/slide/delete-slide/',
                'type': 'post',
                'data': {'id': id},
                success: function (data) {
                    if (data.errors == true) {
                        alert('bạn đã xóa thành công');
                        location.reload();
                    }
                }
            });
        }
    });
    $('#submitcomments').click(function () {
        var idpro = $(this).attr('attr');
        var content = $('textarea').val();
        $.ajax
        ({
            'url': '/fontend/comment',
            'type': 'post',
            'data': {'product_id': idpro, 'content': content},
            success: function (data) {
                console.log(data);
                if (data == false) {
                    window.location.href = '/fontend/login';
                } else {
                    var html = '';
                    $.each(data.data, function (key, value) {
                        html += '<p id="namecomment"><h5 class="edit-magin">';
                        html += data.name;
                        html += ':</h5></p>';
                        html += '<span id="contentconment" >';
                        html += value['content'];
                        html += '</span>';
                        if (data.level == 1 || data.userId == value['user_id']) {
                            html += '<a style="float: right" href="/fontend/delete-comment/';
                            html += value['id'];
                            html += '">delete</a>';
                        }
                        html += '<hr>';
                    });
                    var content = $('textarea').val('');
                    $('.update_conment').html(html);
                    swal("Thông Báo!", "Bạn Đã Gửi Bình Luận Thành Công!", "success");
                }
            }
        });
    });
    $('.button').click(function () {
        var id = $(this).attr('attr');
        $.ajax
        ({
            'url': '/fontend/check-login',
            'type': 'GET',
            success: function (data) {
                if (data.errors == true) {

                    $.ajax
                    ({
                        'url': '/fontend/add-cart',
                        'type': 'GET',
                        'data': {'id': id},
                        success: function (data) {
                            swal({
                                title: data,
                                text: "vui lòng kiểm tra lại trong rỏ hàng của bạn",
                                icon: "success",
                                button: "Tôi Đã Hiểu!",
                            });
                        }
                    });
                } else {
                    window.location.href = "/fontend/login";
                }
            }
        });
    });
    $('.showProduct').click(function () {
        $.ajax
        ({
            'url': '/fontend/show-order',
            'type': 'GET',
            'data': {'id': id},
            success: function (data) {
                var i = 1;
                var total = 0;
                var html = '';
                html += '<table class="table" id="customtable">';
                html += ' <thead>';
                html += '<tr>';
                html += '<th scope="col">stt</th>';
                html += '<th scope="col">name</th>';
                html += '<th scope="col">giá </th>';
                html += '<th scope="col">số lượng</th>';
                html += '<th scope="col">hình ảnh</th>';
                html += '<th scope="col">Thành Tiền</th>';
                html += '</tr>';
                html += '</thead>';
                html += '<tbody>';
                $.each(data, function (key, item) {
                    html += '<tr>';
                    html += '<th scope="row">';
                    html += i++;
                    html += '</th>';
                    html += '<td>'
                    html += item['name'];
                    html += '</td>';
                    html += '<td>'
                    html += String(item['price']).replace(/(.)(?=(\d{3})+$)/g, '$1,');
                    html += '</td>';
                    html += '<td>'
                    html += item['quantity'];
                    html += '</td>';
                    html += '<td> <img style="width:30px" src="/image/';
                    html += item.attributes['image'];
                    html += '" alt=""> </td>';
                    html += '<td>'
                    html += String(item['price'] * item['quantity']).replace(/(.)(?=(\d{3})+$)/g, '$1,');
                    html += 'đ</td>';
                    html += '</tr>';
                    total += item['price'] * item['quantity'];
                });
                html += '</tbody>';
                html += '</table>';
                html += '<b class="btn btn-primary"> Tổng Thành Tiền &nbsp;:&nbsp;';
                html += String(total).replace(/(.)(?=(\d{3})+$)/g, '$1,');
                html += 'đ</b>';
                html += ' <a class="btn btn-success checkout-Pro" href="/fontend/checkout">Thanh toán</a>';
                $('.showajax').html(html);
                $('.showajax').toggle();

            }

        });
    });
    $('.showajax').hide();
    var array = [];
    $(".value-minus").each(function () {
        array.push($(this).data("val"));
    });
    $.each(array, function (index, value) {
        var checkload = Number($('#h' + value).html());
        if (checkload == 1) {
            $('#t' + value).hide();
        }
        ;
    });
    $('.value-plus').click(function () {
        var id = $(this).attr('attr');
        var nums = Number($(this).prev().html());
        var num = nums + 1;
        if (nums == 1) {
            $('#t' + id).show();
        }
        $(this).prev().html(num);
        $.ajax
        ({
            'url': '/fontend/edit-qty',
            'type': 'post',
            'data': {'ids': id, 'qty': num},
            success: function (data) {
                $('#' + id).html(String(data.item.quantity * data.item.price).replace(/(.)(?=(\d{3})+$)/g, '$1,'));
                $('#updateprice').text('Tổng Tiền Của Đơn Hàng=' + String(data.total).replace(/(.)(?=(\d{3})+$)/g, '$1,') + 'đ');
            }

        });


    });
    $('.value-minus').click(function () {
        var id = $(this).attr('attr');
        var nums = Number($(this).next().html());
        var num = nums - 1;
        if (nums == 2) {
            $(this).hide();
        }
        $(this).next().html(num);
        $.ajax
        ({
            'url': '/fontend/edit-qty',
            'type': 'post',
            'data': {'ids': id, 'qty': num},
            success: function (data) {
                $('#' + id).html(String(data.item.quantity * data.item.price).replace(/(.)(?=(\d{3})+$)/g, '$1,'));
                $('#updateprice').text('Tổng Tiền Của Đơn Hàng=' + String(data.total).replace(/(.)(?=(\d{3})+$)/g, '$1,') + 'đ');
            }
        });

    });
    $('.rem').click(function () {
        var id = $(this).attr('attr');

        var check = confirm('bạn có chắc chắn muốn xóa sản phẩm này không?');
        if (check) {
            $.ajax
            ({
                'url': '/fontend/delete-product-checkout',
                'type': 'post',
                'data': {'id': id},
                success: function (data) {
                    if (data.errors == false) {
                        $('.' + id).remove();
                        $('#updateprice').text('Tổng Tiền Của Đơn Hàng=' + String(data.total).replace(/(.)(?=(\d{3})+$)/g, '$1,') + 'đ');
                    }
                }
            });
        }

    });
    $('.deletehistory').click(function () {
        var id = $(this).attr('attr');
        var checks = confirm('bạn có chắc chắn muốn xóa sản phẩm này không?');
        if (checks) {
            $.ajax
            ({
                'url': '/fontend/delete-history',
                'type': 'post',
                'data': {'id': id},
                success: function (data) {
                    if (data.errors == false) {
                        location.reload();
                    } else
                        var check = swal("Thông Báo!", "Đơn hàng này vừa được xác nhận, bạn không thể xóa");

                }
            });
        }

    });
    $("#quantitynumber").validate({
        rules: {
            "quantity": {
                required: true,
                number: true,
                digits: true,
                min: 1,
                maxlength: 3,
            },
        },
        messages: {
            "quantity": {
                required: "Vui lòng nhập số lượng bạn cần mua",
                number: "Hãy nhập số đúng định dạng số",
                digits: "Bạn phải nhập số lượng lớn hơn 0",
                min: "số lượng phải lơn hơn 0",
                maxlength: "vui lòng kiểm tra lại số lượng bạn muốn mua"
            },
        }
    });
    $("#form-search-st").validate({
        rules: {
            "price_min": {

                number: true,
                /*max: function () {
                    return parseInt($('#price-max').val());
                }*/
            },
            "price_max": {

                number: true,
            },
        },
        messages: {
            "price_min": {
                number: "Hãy nhập số đúng định dạng số",
                max: "nhập số tiền kết thúc tìm kiếm lớn hơn số tiền bắt đầu tìm kiếm"
            },
            /*"price_max": {
                number: "Hãy nhập số đúng định dạng số",
            },*/
        }
    });


    $('#confirmcheckout').tooltip();
    $('#notificationcheckout').tooltip();
    $('#detailcheckout').tooltip();
    $('#backorder').tooltip();

    $('#select-cate').change(function () {
        var id = $(this).val();
        $.ajax
        ({
            'url': '/admin/product/ajax-select-list-cate',
            'type': 'get',
            'data': {'id': id},
            success: function (data) {
                $('#select-list-cate').html(data);
            }

        });

    });
    $(document).on('change', '.data-search', function () {
        var startDate1 = $('#startDate').val();
        var startDate = startDate1.split('-');
        var checkDateStrat = (startDate[0] + startDate[1] + startDate[2]);
        var endDate1 = $('#endDate').val();
        var endDate = endDate1.split('-');
        var checkDateEnd = (endDate[0] + endDate[1] + endDate[2]);
        if (startDate == '') {
            checkDateStrat = 0;
        } else if (endDate == '') {
            checkDateEnd = 0;
        }
        /*console.log(checkDateStrat,checkDateEnd);*/
        console.log(checkDateEnd, checkDateStrat);
        if (checkDateEnd < checkDateStrat && startDate != '' && endDate != '') {
            $('#submit-form').hide();
            alert('vui lòng xem lại ngày bắt đầu tìm kiếm phải nhỏ hơn hoặc bằng ngày kết thúc tìm kiếm');
        } else {
            $('#submit-form').show();
        }
    });


    /*    $("form").on("submit", function (e) {
            e.preventDefault();
            $.ajax
            ({

                url:"http://127.0.0.1:8000/admin/statistical/search-statistical-product-form",   // đường dẫn gửi ajax
                method:"POST",
                data:new FormData(this),              // lấy ra tất cả dữ liệu của form để gửi
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,

                success:function (data) {
                    console.log(data);
                }
            })

        });*/


});
