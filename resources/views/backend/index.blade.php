<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Welcome To | Bootstrap Based Admin Template - Material Design</title>
    <!-- Favicon-->
{{--<link rel="icon" href="favicon.ico" type="image/x-icon">--}}

<!-- Google Fonts -->
    {{--<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">--}}
    <link href="{!! asset('backend/css/icon.css') !!}" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{!! asset('backend/plugins/bootstrap/css/bootstrap.css') !!}" rel="stylesheet">

    <!-- Waves Effect Css -->
{{--<link href="{!! asset('backend/plugins/node-waves/waves.css') !!}" rel="stylesheet" />--}}

<!-- Animation Css -->
    <link href="{!! asset('backend/plugins/animate-css/animate.css') !!}" rel="stylesheet"/>

    <!-- Morris Chart Css-->
    <link href="{!! asset('backend/plugins/morrisjs/morris.css') !!}" rel="stylesheet"/>

    <!-- Custom Css -->
    <link href="{!! asset('backend/css/style.css') !!}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{!! asset('backend/css/themes/all-themes.css') !!}" rel="stylesheet"/>
    <link href="{!! asset('backend/css/my.css') !!}" rel="stylesheet">
    <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>
    <style>
        body{
            background-color: #F8F8F8;
        }
    </style>
</head>

<body style="background-color: #F8F8F8" id="newbody" class="theme-red">
@include('backend.layout.header')
@include('backend.layout.sidebar')
@yield('content')
<!-- Jquery Core Js -->

<script src="{!! asset('backend/plugins/jquery/jquery.min.js') !!}"></script>
<link rel="stylesheet" href="{!! asset('backend/jquery-ui-1.12.1/jquery-ui.css') !!}">
<script src="{!! asset('backend/jquery-ui-1.12.1/jquery-ui.js') !!}"></script>

<!-- Bootstrap Core Js -->
<script src="{!! asset('backend/plugins/bootstrap/js/bootstrap.js') !!}"></script>

<!-- Select Plugin Js -->
{{--<script src="{!! asset('backend/plugins/bootstrap-select/js/bootstrap-select.js') !!}"></script>--}}

<!-- Slimscroll Plugin Js -->
<script src="{!! asset('backend/plugins/jquery-slimscroll/jquery.slimscroll.js') !!}"></script>

<!-- Waves Effect Plugin Js -->
<script src="{!! asset('backend/plugins/node-waves/waves.js') !!}"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="{!! asset('backend/plugins/jquery-countto/jquery.countTo.js') !!}"></script>

<!-- Morris Plugin Js -->
<script src="{!! asset('backend/plugins/raphael/raphael.min.js') !!}"></script>
<script src="{!! asset('backend/plugins/morrisjs/morris.js') !!}"></script>

<!-- ChartJs -->
{{--<script src="{!! asset('backend/plugins/chartjs/Chart.bundle.js') !!}"></script>

<!-- Flot Charts Plugin Js -->
<script src="{!! asset('backend/plugins/flot-charts/jquery.flot.js') !!}"></script>
<script src="{!! asset('backend/plugins/flot-charts/jquery.flot.resize.js') !!}"></script>
<script src="{!! asset('backend/plugins/flot-charts/jquery.flot.pie.js') !!}"></script>
<script src="{!! asset('backend/plugins/flot-charts/jquery.flot.categories.js') !!}"></script>
<script src="{!! asset('backend/plugins/flot-charts/jquery.flot.time.js') !!}"></script>--}}

<!-- Sparkline Chart Plugin Js -->
<script src="{!! asset('backend/plugins/jquery-sparkline/jquery.sparkline.js') !!}"></script>

<!-- Custom Js -->
<script src="{!! asset('backend/js/admin.js') !!}"></script>
{{--<script src="{!! asset('backend/js/pages/index.js') !!}"></script>--}}

<!-- Demo Js -->
<script src="{!! asset('backend/js/jquery.validate.min.js') !!}"></script>
<script src="{!! asset('backend/js/demo.js') !!}"></script>
<script src="{!! asset('backend/js/my.js') !!}"></script>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#profile-img").change(function () {
        readURL(this);
    });
</script>

</body>

</html>
