@extends('backend.index')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @if(session('thongbao'))
                                <div style="text-align: center"
                                     class="alert alert-success">{!! session('thongbao') !!}</div>
                            @endif
                            <h2 class="custom">
                                Dách Sách Người Dùng
                            </h2>
                        </div>
                        <div class="body table-responsive">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên</th>
                                    <th>Email</th>
                                    <th>Avatar</th>
                                    <th>Giới Tính</th>
                                    <th>Ngày Sinh</th>
                                    <th>Địa Chỉ</th>
                                    <th>Điện Thoại</th>
                                    <th>Chức vụ</th>
                                    <th>Ngày tạo</th>
                                    <th>Thao tác</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $key => $value)
                                    <tr class="">
                                        <th scope="row">{!! ((($users->currentPage()*10)-10)+1)+$key !!}</th>
                                        <td>{!! $value->name !!}</td>
                                        <td>{!! $value->email !!}</td>
                                        <td><img style="width: 70px; height: 80px"
                                                 src="{!! asset('image/'.$value->avatar) !!}" alt=""></td>
                                        @if($value->gender == 1)
                                            <td>Nữ</td>
                                        @else
                                            <td>Nam</td>
                                        @endif
                                        <td>{!! date('d-m-Y', strtotime($value->birthday ));!!}</td>
                                        <td>{!! $value->address !!}</td>
                                        <td>{!! $value->phone !!}</td>
                                        @if($value->lever ==1)
                                            <td>Admin</td>
                                        @else
                                            <td>Người dùng</td>
                                        @endif
                                        <td>{!! $value->created_at !!}</td>
                                        <td>
                                            <a href="{!! route('UserBackend',['id'=>$value->id]) !!}"
                                               class="glyphicon glyphicon-edit">&nbsp;</a>
                                            <span id="" attr="{!! $value->id  !!}"
                                                  class="glyphicon glyphicon-trash deluser"></span>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {!! $users->links() !!}
                </div>
            </div>
        </div>
    </section>
@endsection