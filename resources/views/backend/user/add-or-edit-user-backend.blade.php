@extends('backend.index')
@section('content')
    <section class="content">
        <div class="container-fluid">
            @if(session('thongbao'))
                <div class="alert alert-success">
                    {!! session('thongbao') !!}
                </div>
        @endif
        <!-- Basic Validation -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @if(isset($user))
                                <h2 class="custom1">Kiểm Tra Lại Dữ Liệu Bạn Cần Sửa</h2>
                            @else
                                <h2 class="custom1">Nhập Thông Tin Đăng kí</h2>
                            @endif
                        </div>
                        <div class="body">
                            @if(isset($user))
                                <form id="" action="{!! route('PostUserBackend',['id'=>$user->id]) !!}" method="POST"
                                      enctype="multipart/form-data">
                                    @else
                                        <form id="" action="{!! route('PostUserBackend') !!}" method="POST"
                                              enctype="multipart/form-data">
                                            @endif
                                            @csrf
                                            <div class="form-group form-float">
                                                <div class="form-line ">
                                                    <input type="text" class="form-control" name="name"
                                                           @if(isset($user)) value="{!! $user->name !!}"
                                                           @else value="{!! old('name') !!}" @endif>
                                                    <label class="form-label">Name</label>
                                                </div>
                                            </div>
                                            @if($errors->has('name'))
                                                <div class="alert-errors">{!! $errors->first('name') !!}</div>
                                            @endif
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="email" class="form-control" name="email"
                                                           @if(isset($user)) value="{!! $user->email  !!}"
                                                           @else value="{!! old('email') !!}" @endif>
                                                    <label class="form-label">Email</label>
                                                    @if($errors->has('email'))
                                                        <div class="alert-errors">{!! $errors->first('email') !!}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="password" class="form-control" name="password"
                                                           value="">
                                                    <label class="form-label">password</label>
                                                </div>
                                            </div>
                                            @if($errors->has('password'))
                                                <div class="alert-errors">{!! $errors->first('password') !!}</div>
                                            @endif
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="password" class="form-control" name="passwordold">
                                                    <label class="form-label">Nhập Lại password</label>
                                                </div>
                                            </div>
                                            @if($errors->has('passwordold'))
                                                <div class="alert-errors">{!! $errors->first('passwordold') !!}</div>
                                            @endif
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="phone"
                                                           @if(isset($user)) value="{!! $user->phone  !!}"
                                                           @else value="{!! old('phone') !!}" @endif>
                                                    <label class="form-label">Nhập Số Điện Thoại Của bạn</label>
                                                </div>
                                            </div>
                                            @if($errors->has('phone'))
                                                <div class="alert-errors">{!! $errors->first('phone') !!}</div>
                                            @endif
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="address"
                                                           @if(isset($user)) value="{!! $user->address  !!}"
                                                           @else value="{!! old('address') !!}" @endif>
                                                    <label class="form-label">Nhập Địa Chỉ Của bạn</label>
                                                </div>
                                            </div>
                                            @if($errors->has('address'))
                                                <div class="alert-errors">{!! $errors->first('address') !!}</div>
                                            @endif
                                            <div>
                                                <label class="form-group">+Chọn Ngày Sinh Của bạn</label>
                                                <input type="date" name="birthday"
                                                       @if(isset($user)) value="{!! $user->birthday  !!}"
                                                       @else value="{!! old('birthday') !!}" @endif>
                                            </div>
                                            @if($errors->has('birthday'))
                                                <div class="alert-errors">{!! $errors->first('birthday') !!}</div>
                                            @endif
                                            <div class="form-group">
                                                <div class="form-control">
                                                    <label class="form-label">+Chọn ảnh địa diện</label>
                                                    <input type="file" id="profile-img" class="form-control"
                                                           name="avatar">
                                                    <lable><img src="" id="profile-img-tag" width="200px"/></lable>
                                                </div>
                                            </div>
                                            @if(isset($user))
                                                <img style="width: 70px" src="{!! asset('image/'.$user->avatar) !!}"
                                                     alt="">
                                            @endif
                                            @if($errors->has('avatar'))
                                                <div class="alert-errors">{!! $errors->first('avatar') !!}</div>
                                            @endif
                                            <div class="form-group ">
                                                <label class="form-control">+Chọn giới tính</label>
                                                <input type="radio" name="gender"
                                                       @if(isset($user))
                                                       @if($user->gender==0)
                                                       {!! "checked" !!}
                                                       @endif
                                                       @elseif(old('gender') ==0)
                                                       {!! "checked" !!}
                                                       @endif
                                                       id="male" class="with-gap" value="0">
                                                <label for="male">Nam</label>
                                                <input type="radio" name="gender"
                                                       @if(isset($user))
                                                       @if($user->gender==1)
                                                       {!! "checked" !!}
                                                       @endif
                                                       @elseif(old('gender') ==1)
                                                       {!! "checked" !!}
                                                       @endif
                                                       id="female" class="with-gap" value="1">
                                                <label for="female" class="m-l-20">Nữ</label>
                                            </div>
                                            @if($errors->has('gender'))
                                                <div class="alert-errors">{!! $errors->first('gender') !!}</div>
                                            @endif
                                            @if(isset($user))
                                                <button class="btn btn-primary waves-effect custom4" type="submit">Sửa
                                                    Thông Tin
                                                </button>
                                            @else
                                                <button class="btn btn-primary waves-effect custom4" type="submit">Đăng
                                                    kí
                                                </button>
                                            @endif
                                        </form>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection