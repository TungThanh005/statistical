@extends('backend.index')
@section('content')
    <section class="content">
        <div class="container">
            <div class="block-header">

            </div>
            <h2 style="color: #0D47A1">Chọn số liệu thống kê theo danh sách</h2>
            <h2>
                <hr style="border: #0D47A1 solid 5px">
            </h2>
            <!-- Basic Validation -->
            <div class="row clearfix fix-padding">
                <b>1.</b><a href="{{ route('statisticalProductForm') }}">Thống kê theo số lượng bán</a>
            </div>
            <div class="row clearfix fix-padding">
                <b>2.</b><a href="{{ route('statisticalProductViewForm') }}">Thống kê theo lượt xem sản phẩm</a>
            </div>
            <div class="row clearfix fix-padding">
                <b>3.</b><a href="{{ route('statisticalProductpriceForm') }}">Thống kê theo giá thành sản phẩn</a>
            </div>
        </div>
    </section>
@endsection