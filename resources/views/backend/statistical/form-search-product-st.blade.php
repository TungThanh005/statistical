@extends('backend.index')
@section('content')
    <section class="content">
        <div class="container">
            <h2>Chọn số liệu thống kê</h2>
            <form action="" method="" id="form-search-st">
                @csrf
                <div class="form-group form-float">
                    <lable>ngày bắt đầu</lable>
                    <input type="date" name="startDate" id="startDate" class="data-search">
                    <lable>ngày kết thúc</lable>
                    <input type="date" name="endDate" id="endDate" class="data-search">
                </div>
                <div class="form-group form-float">
                    <lable>Chọn danh mục chính</lable>
                    <select name="select_cate" id="select-cate">
                        <option value="">xin mời chọn</option>
                        @foreach($cates as $cate)
                            <option value="{{ $cate->id }}">{{ $cate->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group form-float">
                    <lable>Chọn danh mục con</lable>
                    <select name="select_listCate" id="select-list-cate">
                        <option value="">Xin mời chọn</option>
                    </select>
                </div>
                <div class="form-group form-float">
                    <lable>Chọn mức giá</lable>
                    <div>
                        <lable>từ</lable>
                        <input type="text" name="price_min" id="price-min">
                        <lable>đến</lable>
                        <input type="text" name="price_max" id="price-max">
                    </div>
                </div>
                <div>
                    <input class="btn btn-success" type="submit" id="submit-form" value="Thống kê">
                </div>
            </form>
            <div>
                <p>
                <h2>Biểu đồ số liệu thống kê sản phẩm theo số lượng bán ra</h2></p>
                <canvas id="chart" width="1000  " height="600"></canvas>
                <div class="col-md-6" id="table"></div>
            </div>
        </div>
        <script src='{{ asset('js/Chart.min.js') }}'></script>
        <script src='{{ asset('backend/js/jquery-3.3.1.min.js') }}'></script>
        <script>
            $("form").on("submit", function (e) {
                e.preventDefault();
                $.ajax
                ({
                    url: "http://127.0.0.1:8000/admin/statistical/search-statistical-product-form",   // đường dẫn gửi ajax
                    method: "POST",
                    data: new FormData(this),              // lấy ra tất cả dữ liệu của form để gửi
                    dataType: 'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        if(data[0].length == 0){
                            alert('không có dữ liệu trả về vui lòng xem kiểm tra lại tìm kiếm');
                        }
                        var buyerData = {
                            labels: data[0],
                            datasets: [
                                {
                                    fillColor: "rgba(172,194,132,0.4)",
                                    strokeColor: "#ACC26D",
                                    pointColor: "#fff",
                                    pointStrokeColor: "#9DB86D",
                                    data: data[1]
                                }
                            ]
                        }
                        // get line chart canvas
                        var buyers = document.getElementById('chart').getContext('2d');
                        // draw line chart
                        new Chart(buyers).Line(buyerData);
                        var html = '';
                        html += '<h4>Bảng Thống kê chi tiết số lượng sản phẩm đã bán</h4>'
                        html += '<table class="table" id="customtable">';
                        html += ' <thead>';
                        html += '<tr>';
                        html += '<th scope="col">Tên sản phẩm</th>';
                        html += '<th scope="col">Số lượng đã bán</th>';
                        html += '</tr>';
                        html += '</thead>';
                        html += '<tbody>';
                        for (var i = 0; i < data[0].length; i++) {
                            html += '<tr>';
                            html += '<td>'
                            html += data[0][i];
                            html += '</td>';
                            html += '<td>'
                            html += String(data[1][i]).replace(/(.)(?=(\d{3})+$)/g, '$1,');
                            html += '</td>';
                            html += '</tr>';
                        }
                        html += '</tbody>';
                        html += '</table>';
                        $('#table').html(html);
                    }
                })
            });
        </script>
    </section>
@endsection