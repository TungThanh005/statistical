<!DOCTYPE html>
<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>
<div class="container">
<div style="width: 100%" class="row">
    @if(isset($chart))
        <div style="width: 100%" class="col-md-5">
            {!! $chart->container() !!}
        </div>
        {!! $chart->script() !!}
    @endif
    <div class="col-md-1"></div>
    @if(isset($chartSum))
        <div style="width: 100%" class="col-md-5">
            {!! $chartSum->container() !!}
        </div>
        {!! $chartSum->script() !!}
    @endif

</div>
<div style="width: 100%" class="row">
    @if(isset($chartAge))
        <div style="width: 100%" class="col-md-5">
            {!! $chartAge->container() !!}
        </div>
        {!! $chartAge->script() !!}
    @endif
    <div class="col-md-1"></div>
    @if(isset($chartGender))
        <div style="width: 100%" class="col-md-10">
            {!! $chartGender->container() !!}
        </div>
        {!! $chartGender->script() !!}
    @endif

</div>
</div>
 
</body>
 
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js" charset="utf-8"></script>
</html>