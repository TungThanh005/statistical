<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col align-self-center">
            <h4>Thống kê số lượng bán theo ngày</h4>
            <h2 style="color: red" id="notifi"></h2>
            <h5>Từ ngày</h5>
            <input type="date" id="startDate" class="dataSearch">
            <h5>Đến ngày</h5>
            <input type="date" id="endDate" class="dataSearch">
            <h4 class="btn btn-info" id="search">Tìm kiếm</h4>
    </div>
    </div>
</div>

<div>
    <canvas id="chart" width="800" height="600"></canvas>
</div>
</body>
<script src='{{ asset('js/Chart.min.js') }}'></script>
<script src='{{ asset('backend/js/jquery-3.3.1.min.js') }}'></script>
<script>

    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#search').hide();
        $(document).on('change', '.dataSearch', function () {
            var startDate1 = $('#startDate').val();
            var startDate = startDate1.split('-');
            var checkDateStrat = (startDate[0] + startDate[1] + startDate[2]);
            var endDate1 = $('#endDate').val();
            var endDate = endDate1.split('-');
            var checkDateEnd = (endDate[0] + endDate[1] + endDate[2]);
            if (startDate == '') {
                checkDateStrat = 0;
            } else if (endDate == '') {
                checkDateEnd = 0;
            }
            /*console.log(checkDateStrat,checkDateEnd);*/
            if (checkDateEnd >= checkDateStrat) {
                $('#search').show();
                $('#search').on('click', function () {
                    $.ajax
                    ({
                        'url': '/admin/statistical/search-date-the-period/',
                        'type': 'post',
                        'data': {'startDate': startDate1, 'endDate': endDate1},
                        success: function (data) {
                            console.log(data.length);
                            if (data.length === 0) {
                                $('#notifi').html('không có kết quả tìm kiếm');
                            }else {
                                $('#notifi').hide();
                            }
                            var labels = [];
                            var datas = [];
                            $.each(data, function (keys, items) {
                                labels.push(items['orderdetail_product']['name']);
                                datas.push(items['product_id']);
                            });
                            var buyerData = {
                                labels: labels,
                                datasets: [
                                    {
                                        fillColor: "rgba(172,194,132,0.4)",
                                        strokeColor: "#ACC26D",
                                        pointColor: "#fff",
                                        pointStrokeColor: "#9DB86D",
                                        data: datas
                                    }
                                ]
                            }
                            // get line chart canvas
                            var buyers = document.getElementById('chart').getContext('2d');

                            // draw line chart
                            new Chart(buyers).Line(buyerData);
                        }
                    });
                });
            } else if (checkDateEnd < checkDateStrat && startDate != '' && endDate != '') {
                $('#search').hide();
                alert('vui lòng xem lại ngày bắt đầu tìm kiếm phải nhỏ hơn hoặc bằng ngày kết thúc tìm kiếm');
            }
        });

    });
</script>
</html>