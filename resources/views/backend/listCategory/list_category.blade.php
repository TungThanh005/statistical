@extends('backend.index')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @if(session('thongbao'))
                                <div class="alert alert-success">{!! session('thongbao') !!}</div>
                            @endif
                            @if(session('thongBao'))
                                <div class="alert alert-success">{!! session('thongBao') !!}</div>
                            @endif
                            <h2 class="custom">
                                Dách Sách danh mục con
                            </h2>
                        </div>
                        <div class="body table-responsive">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên</th>
                                    <th>danh mục</th>
                                    <th>status</th>
                                    <th>Ngày tạo</th>
                                    <th>Ngày sửa</th>
                                    <th>Thao tác</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($list as $key => $value)
                                    <tr class="">
                                        <th scope="row">{!! ((($list->currentPage()*10)-10)+1)+$key !!}</th>
                                        <td>{!! $value->name !!}</td>
                                        <td>
                                            @if(!is_null($value->listcate_cate))
                                                {!! $value->listcate_cate->name !!}
                                            @endif
                                        </td>
                                        @if($value->status==1)
                                            <td>Hiển Thị</td>
                                        @else
                                            <td>Ẩn</td>
                                        @endif
                                        <td>{!! $value->created_at !!}</td>
                                        <td>{!! $value->updated_at !!}</td>
                                        <td>
                                            <a href="{!! route('AddEditListCate',['id'=>$value->id]) !!}"
                                               class="glyphicon glyphicon-edit">&nbsp;</a>
                                            <span id="deListCate" attr="{!! $value->id  !!}"
                                                  class="glyphicon glyphicon-trash deListCate"></span>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {!! $list->links() !!}
                </div>
            </div>
        </div>
    </section>
@endsection