@extends('backend.index')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
            </div>
            <!-- Basic Validation -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @if(isset($listcate))
                                <h2 class="custom1">Nhập Dữ Liệu Cần Sửa</h2>
                            @else
                                <h2 class="custom1">Nhập Dữ Liệu Cần Thêm mới</h2>
                            @endif

                        </div>
                        <div class="body">
                            @if(isset($listcate))
                                <form id="" action="{!! route('postAddOrEditListCate',['id'=>$listcate->id]) !!}"
                                      method="POST">
                                    @else
                                        <form id="" action="{!! route('postAddOrEditListCate') !!}" method="POST">
                                            @endif
                                            @csrf
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="name"
                                                           @if(isset($listcate)) value="{!! $listcate->name !!}"
                                                           @else value="{!! old('name') !!}" @endif>
                                                    <label class="form-label">Nhập tên Danh Mục Sản Phẩm Bạn Muốn
                                                        Sửa</label>
                                                </div>
                                            </div>
                                            @if($errors->has('name'))
                                                <div class="alert-errors">{!! $errors->first('name') !!}</div>
                                            @endif
                                            <div class="form-group">
                                                <label for="disabledSelect">Chọn danh mục</label>
                                                <select id="disabledSelect" class="form-control" name="category_id">
                                                    <option value="">xin mời chọn</option>
                                                    @if(!is_null($cates))
                                                        @foreach($cates as $cate)
                                                            <option value="{!! $cate->id !!}"
                                                            @if(isset($listcate))
                                                                @if($listcate->listcate_cate->id==$cate->id)
                                                                    {!! "selected" !!}
                                                                        @endif
                                                                    @else
                                                                @if(old('category_id')==$cate->id)
                                                                    {!! "selected" !!}
                                                                        @endif
                                                                    @endif
                                                            >{!! $cate->name !!}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            @if($errors->has('category_id'))
                                                <div class="alert-errors">{!! $errors->first('category_id') !!}</div>
                                            @endif
                                            <div class="form-group">
                                                <label class="form-control">Trạng thái</label>
                                                <input type="radio" name="status" id="female" class="with-gap" value="1"
                                                @if(isset($listcate))
                                                    @if($listcate->status==1)
                                                        {!! "checked" !!}
                                                            @endif
                                                        @elseif(old('status')==1)
                                                    {!! "checked" !!}
                                                        @endif >
                                                <label for="female" class="m-l-20">Hiện</label>
                                                <input type="radio" name="status" id="male" class="with-gap" value="0"
                                                @if(isset($listcate))
                                                    @if($listcate->status==0)
                                                        {!! "checked" !!}
                                                            @endif
                                                        @elseif(old('status')==0)
                                                    {!! "checked" !!}
                                                        @endif >
                                                <label for="male">Ẩn</label>
                                            </div>
                                            @if($errors->has('gender'))
                                                <div class="alert-errors">{!! $errors->first('gender') !!}</div>
                                            @endif
                                            @if(isset($listcate))
                                                <button class="btn btn-primary waves-effect custom4" type="submit">Sửa
                                                </button>
                                            @else
                                                <button class="btn btn-primary waves-effect custom4" type="submit">Thêm
                                                    Mới
                                                </button>
                                            @endif
                                        </form>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection