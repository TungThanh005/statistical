@extends('backend.index')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
            </div>
            <!-- Basic Validation -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @if(isset($slide))
                                <h2 class="custom1">Nhập Dữ Liệu Cần Sửa</h2>
                            @else
                                <h2 class="custom1">Nhập Dữ Liệu Cần Thêm Mới</h2>
                            @endif
                        </div>
                        <div class="body">
                            @if(isset($slide))
                                <form id="" action="{!! route('postAddOrditSlide',['id'=>$slide->id]) !!}" method="POST"
                                      enctype="multipart/form-data">
                                    @else
                                        <form id="" action="{!! route('postAddOrditSlide') !!}" method="POST"
                                              enctype="multipart/form-data">
                                            @endif
                                            @csrf
                                            <div class="form-group">
                                                <div class="form-control">
                                                    <label class="form-label">+Chọn ảnh slide</label>
                                                    <input type="file" class="form-control" name="image">
                                                </div>

                                            </div>
                                            @if(isset($slide))
                                                <div>
                                                    <img style="width: 60px" src="{!! asset('image/'.$slide->image) !!}"
                                                         alt="">
                                                </div>
                                            @endif
                                            @if($errors->has('image'))
                                                <div class="alert-errors">{!! $errors->first('image') !!}</div>
                                            @endif
                                            <div class="form-group">
                                                <label class="form-control">Trạng thái</label>
                                                <input type="radio" name="status" id="female" class="with-gap" value="1"
                                                @if(isset($slide))
                                                    @if($slide->status==1)
                                                        {!! "checked" !!}
                                                            @endif
                                                        @elseif(old('status')==1)
                                                    {!! "checked" !!}
                                                        @endif >
                                                <label for="female" class="m-l-20">Hiện</label>
                                                <input type="radio" name="status" id="male" class="with-gap" value="0"
                                                @if(isset($slide))
                                                    @if($slide->status==0)
                                                        {!! "checked" !!}
                                                            @endif
                                                        @elseif(old('status')==0)
                                                    {!! "checked" !!}
                                                        @endif >
                                                <label for="male">Ẩn</label>
                                            </div>
                                            @if(isset($slide))
                                                <button class="btn btn-primary waves-effect custom4" type="submit">Sửa
                                                    slide
                                                </button>
                                            @else
                                                <button class="btn btn-primary waves-effect custom4" type="submit">Tạo
                                                    mới
                                                </button>
                                            @endif
                                        </form>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection