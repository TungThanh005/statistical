@extends('backend.index')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @if(session('thongbao'))
                                <div class="alert alert-success">{!! session('thongbao') !!}</div>
                            @endif
                            <h2 class="custom">
                                Dách Sách danh mục
                            </h2>
                        </div>
                        <div class="body table-responsive">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên</th>
                                    <th>status</th>
                                    <th>Ngày tạo</th>
                                    <th>Ngày sửa</th>
                                    <th>Thao tác</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cate as $key => $value)
                                    <tr class="">
                                        <th scope="row">{!! ((($cate->currentPage()*10)-10)+1)+$key !!}</th>
                                        <td>{!! $value->name !!}</td>
                                        @if($value->status==1)
                                            <td>Hiển Thị</td>
                                        @else
                                            <td>Ẩn</td>
                                        @endif
                                        <td>{!! $value->created_at !!}</td>
                                        <td>{!! $value->updated_at !!}</td>
                                        <td>
                                            <a href="{!! route('AddEditCate',['id'=>$value->id]) !!}"
                                               class="glyphicon glyphicon-edit">&nbsp;</a>
                                            <span id="" attr="{!! $value->id  !!}"
                                                  class="glyphicon glyphicon-trash dellCate"></span>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {!! $cate->links() !!}
                </div>
            </div>
        </div>
    </section>
@endsection