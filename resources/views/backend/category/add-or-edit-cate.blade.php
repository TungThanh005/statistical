@extends('backend.index')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
            </div>
            <!-- Basic Validation -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @if(isset($cate))
                                <h2 class="custom1">Nhập Dữ Liệu Cần Sửa</h2>
                            @else
                                <h2 class="custom">Nhập Dữ Liệu Cần Thêm Mới</h2>
                            @endif
                        </div>
                        <div class="body">
                            @if(isset($cate))
                                <form id="" action="{!! route('postAddOrEditCate',['id'=>$cate->id]) !!}" method="POST">
                                    @else
                                        <form id="" action="{!! route('postAddOrEditCate') !!}" method="POST">
                                            @endif
                                            @csrf
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="name"
                                                           @if(isset($cate)) value="{!! $cate->name !!}"
                                                           @else value="{!! old('name') !!}" @endif>
                                                    <label class="form-label">Nhập tên Danh Mục Sản Phẩm Bạn Muốn
                                                        Sửa</label>
                                                </div>
                                            </div>
                                            @if($errors->has('name'))
                                                <div class="alert-errors">{!! $errors->first('name') !!}</div>
                                            @endif
                                            <div class="form-group">
                                                <label class="form-control">Trạng thái</label>
                                                <input type="radio" checked name="status" id="female" class="with-gap"
                                                       value="1"
                                                @if(isset($cate))
                                                    @if($cate->status==1)
                                                        {!! "checked" !!}
                                                            @endif
                                                        @elseif(old('status')==1)
                                                    {!! "checkked" !!}
                                                        @endif
                                                >
                                                <label for="female" class="m-l-20">Hiện</label>
                                                <input type="radio" name="status"
                                                       @if(isset($cate))
                                                       @if($cate->status==0)
                                                       {!! "checked" !!}
                                                       @endif
                                                       @elseif(old('status')==0)
                                                       {!! "checkked" !!}
                                                       @endif
                                                       id="male" class="with-gap" value="0">
                                                <label for="male">Ẩn</label>
                                            </div>
                                            @if($errors->has('status'))
                                                <div class="alert-errors">{!! $errors->first('status') !!}</div>
                                            @endif
                                            @if(isset($cate))
                                                <button class="btn btn-primary waves-effect custom4" type="submit">Sửa
                                                </button>
                                            @else
                                                <button class="btn btn-primary waves-effect custom4" type="submit">Thêm
                                                    Mới
                                                </button>
                                            @endif
                                        </form>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection