@extends('backend.index')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @if(session('thongbao'))
                                <div style="text-align: center"
                                     class="alert alert-success">{!! session('thongbao') !!}</div>
                            @endif
                            <h2 class="custom">
                                Dách Sách Sản Phẩm
                            </h2>
                        </div>
                        <div class="body table-responsive">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên</th>
                                    <th>Giá</th>
                                    <th>Avatar</th>
                                    <th>danh mục cha</th>
                                    <th>danh mục con</th>
                                    <th>lượt xem</th>
                                    <th>Trạng thái</th>
                                    <th>sale</th>
                                    <th>Từ</th>
                                    <th>Đến</th>

                                    <th>Thao tác</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($pro as $key => $value)
                                    <tr class="">
                                        <th scope="row">{!! ((($pro->currentPage()*10)-10)+1)+$key !!}</th>
                                        <td>{!! $value->name !!}</td>
                                        <td>{!! number_format($value->price,0) !!}</td>
                                        <td><img style="width: 70px; height: 80px"
                                                 src="{!! asset('image/'.$value->image) !!}" alt=""></td>
                                        <td>{!! $value->category($value->listCategory_id)->name !!}</td>
                                        <td>
                                            @if(!is_null($value->pro_listcate))
                                                {!! $value->pro_listcate->name !!}
                                            @endif
                                        </td>
                                        <td>{!! $value->view !!}</td>
                                        @if($value->status ==1)
                                            <td>hiện</td>
                                        @else
                                            <td>ẩn</td>
                                        @endif
                                        <td>
                                            @if(!is_null($value->product_sale))
                                                {!! $value->product_sale->dis_count !!}%
                                            @else
                                                {!! '0' !!}
                                            @endif
                                        </td>
                                        <td>
                                            @if(!is_null($value->product_sale))
                                                {!! $value->product_sale->start_at !!}
                                            @else
                                                {!! '0' !!}
                                            @endif
                                        </td>
                                        <td>
                                            @if(!is_null($value->product_sale))
                                                {!! $value->product_sale->end_at !!}
                                            @else
                                                {!! '0' !!}
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{!! route('editProduct',['id'=>$value->id]) !!}"
                                               class="glyphicon glyphicon-edit">&nbsp;</a>
                                            <span id="" attr="{!! $value->id  !!}"
                                                  class="glyphicon glyphicon-trash delpro"></span>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {!! $pro->links() !!}
                </div>
            </div>
        </div>
    </section>
@endsection