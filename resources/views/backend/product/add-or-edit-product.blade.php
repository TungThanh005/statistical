@extends('backend.index')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
            </div>
            <!-- Basic Validation -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @if(isset($pro))
                                <h5 class="custom1">Nhập Lại Dữ Liệu Sản Phẩm Cần Sửa</h5>
                            @else
                                <h5 class="custom">Nhập Dữ Liệu Cần Thêm Mới</h5>
                            @endif
                        </div>
                        <div class="body">
                            @if(isset($pro))
                                <form id="" action="{!! route('postAddOrEditProduct',['id'=>$pro->id]) !!}"
                                      method="POST"
                                      enctype="multipart/form-data">
                                    @else
                                        <form id="" action="{!! route('postAddOrEditProduct') !!}" method="POST"
                                              enctype="multipart/form-data">
                                            @endif
                                            @csrf
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="name"
                                                           @if(isset($pro)) value="{!! $pro->name !!}"
                                                           @else value="{!! old('name') !!}" @endif >
                                                    <label class="form-label">Name</label>
                                                </div>
                                            </div>
                                            @if($errors->has('name'))
                                                <div class="alert-errors">{!! $errors->first('name') !!}</div>
                                            @endif
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="price"
                                                           @if(isset($pro)) value="{!! $pro->price !!}"
                                                           @else value="{!! old('price') !!}" @endif>
                                                    <label class="form-label">Nhập Giá Sản phẩm</label>
                                                    @if($errors->has('price'))
                                                        <div class="alert-errors">{!! $errors->first('price') !!}</div>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="agileinfo_single">
                                                <h5>Mô Tả Sản Phẩm</h5>
                                                <textarea rows="4" name="description" style="width: 100%">
                                                    @if(isset($pro)) {!! $pro->description !!} @else {!! old('description') !!}@endif
                                                </textarea>
                                            </div>
                                            @if($errors->has('description'))
                                                <div class="alert-errors">{!! $errors->first('description') !!}</div>
                                            @endif
                                            <div class="form-group">
                                                <label for="disabledSelect">+Chọn danh mục</label>
                                                <select id="llist" class="form-control cate"
                                                        name="category" @if(isset($pro)) {!! "attr= $pro->id" !!} @endif>
                                                    <option value=""><----xin mời chọn----></option>
                                                    @if(!is_null($cates))
                                                        @foreach($cates as $value)
                                                            <option
                                                                    @if(isset($pro))
                                                                    @if($value->id==$catelist->id) {!! "selected" !!} @endif
                                                                    @elseif(old('category')==$value->id)
                                                                    {!! "selected" !!}
                                                                    @endif
                                                                    value="{!! $value->id !!}">{!! $value->name !!}
                                                            </option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            @if($errors->has('category'))
                                                <div class="alert-errors">{!! $errors->first('category') !!}</div>
                                            @endif
                                            <div class="form-group">
                                                <label for="disabledSelect">+Chọn danh mục con</label>
                                                <select id="llist" class="form-control listacte" name="listCategory_id"
                                                        @if(old('listCategory_id')) attr="{!! old('listCategory_id') !!} @endif">
                                                    @if(old('listCategory_id'))
                                                        @foreach($listcate as $value )
                                                            @if(old('listCategory_id') == $value->id)
                                                                <option selected
                                                                        value="{!! $value->id !!}">{!! $value->name !!}</option>
                                                            @endif
                                                            @if(old('listCategory_id') != $value->id)
                                                                <option value="{!! $value->id !!}">{!! $value->name !!}</option>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <option value=""><----xin mời chọn----></option>
                                                    @endif
                                                </select>
                                            </div>
                                            @if($errors->has('listCategory_id'))
                                                <div class="alert-errors">{!! $errors->first('listCategory_id') !!}</div>
                                            @endif
                                            <div class="form-group">
                                                <div class="form-control">
                                                    <label class="form-label">+Sale('Để Trống Nếu Sản Phẩm Này Không
                                                        Sale'):</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <select name="dis_count"
                                                        class="dis_count oldDate1 @if(isset($pro)) {!! 'hideTextDate' !!} @endif">
                                                    <option value="">--Chọn Giá Sale--</option>
                                                    @for($i=1;$i < 100 ;$i++)
                                                        <option value="{!! $i !!}"
                                                        @if(old('dis_count')== $i)
                                                            {!! "selected" !!}
                                                                @elseif(isset($pro))
                                                            @if(isset($sale))
                                                                @if($sale->dis_count== $i)
                                                                    {!! "selected" !!}
                                                                        @endif
                                                                    @endif
                                                                @endif
                                                        >{!! $i !!}%
                                                        </option>
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="form-group {{--hideEdit--}}  @if(old('dis_count')) {!! "oldDate" !!} @else {!! "start_at" !!} @endif "
                                                 @if(isset($sale)) @if(!is_null($sale)) id="start" @endif @endif>
                                                <div class="form-control">
                                                    <label class="form-label">-Ngày Bắt Đầu:</label><br>
                                                    <input type="date" name="start_at"
                                                           @if(isset($pro) || old('end_at') || old('start_at'))
                                                           id="datesale"
                                                           @if(isset($sale))
                                                           value="{!! $sale->start_at !!}"
                                                           @endif
                                                           @else
                                                           id="thanh"
                                                           @endif
                                                           @if(old('start_at'))
                                                           value="{!! old('start_at') !!}"
                                                            @endif
                                                    >
                                                </div>
                                            </div>
                                            @if($errors->has('start_at'))
                                                <div class="alert-errors">{!! $errors->first('start_at') !!}</div>
                                            @endif
                                            <div class="form-group {{--hideEdit--}} @if(old('dis_count')) {!! "oldDate" !!} @else {!! "end_at" !!} @endif "
                                                 @if(isset($sale)) @if(!is_null($sale)) id="end" @endif @endif>
                                                <div class="form-control">
                                                    <label class="form-label">-Ngày Kết thúc:</label><br>
                                                    <input type="date" name="end_at" id="tung"
                                                           @if(isset($pro))
                                                           @if(isset($sale))
                                                           value="{!! $sale->end_at !!}"
                                                           @endif
                                                           @endif
                                                           @if(old('end_at'))
                                                           value="{!! old('end_at') !!}"
                                                            @endif >
                                                    <label style="color: red" id="customDate"></label>
                                                </div>
                                            </div>
                                            @if($errors->has('end_at'))
                                                <div class="alert-errors">{!! $errors->first('end_at') !!}</div>
                                            @endif
                                            <div class="form-group">
                                                <div class="form-control">
                                                    <label class="form-label">+Chọn ảnh Sản Phẩm</label>
                                                    <input type="file" class="form-control" name="images">
                                                </div>
                                            </div>
                                            @if($errors->has('images'))
                                                <div class="alert-errors">{!! $errors->first('images') !!}</div>
                                            @endif
                                            @if(isset($pro))
                                                <div>
                                                    <img style="width:70px;" src="{!! asset('image/'.$pro->image) !!}"
                                                         alt="">
                                                </div>
                                            @endif
                                            <div class="form-group">
                                                <label class="form-control">Trạng thái</label>
                                                <input type="radio" name="status" id="an" class="with-gap" value="1"
                                                @if(isset($pro))
                                                    @if($pro->status==1)
                                                        {!! "checked" !!}
                                                            @endif
                                                        @elseif(old('status')==1)
                                                    {!! "checked" !!}
                                                        @endif >
                                                <label for="an" class="m-l-20">Hiện</label>
                                                <input type="radio" name="status" id="hien" class="with-gap" value="0"
                                                @if(isset($pro))
                                                    @if($pro->status==0)
                                                        {!! "checked" !!}
                                                            @endif
                                                        @elseif(old('status')==0)
                                                    {!! "checked" !!}
                                                        @endif >
                                                <label for="hien">Ẩn</label>
                                            </div>
                                            @if(isset($pro))
                                                <button class="btn btn-primary waves-effect custom4" type="submit">Sửa
                                                    Sản Phẩm
                                                </button>
                                            @else
                                                <button class="btn btn-primary waves-effect custom4" type="submit">Thêm
                                                    Sản Phẩm
                                                </button>
                                            @endif
                                        </form>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection