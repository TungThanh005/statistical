@extends('backend.index')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @if(session('thongbao'))
                                <div class="alert alert-success">{!! session('thongbao') !!}</div>
                            @endif
                            <h2 class="custom">
                                Thông Tin Chi Tiết Đơn Hàng
                            </h2>
                        </div>
                        <div class="body table-responsive ">
                            <h1 style="float: right"><a id="backorder"
                                                        class="btn btn-primary glyphicon glyphicon-arrow-left"
                                                        title="Quay lại" href="{!! route('listOder') !!}"></a></h1>
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>sản phẩm</th>
                                    <th>hình ảnh</th>
                                    <th>s.lượng</th>
                                    <th>đơn giá</th>
                                    <th>thành tiền</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($order as $key => $value)
                                    <tr class="">
                                        <th class="buttuom" scope="row">{!! $key+1 !!}</th>
                                        <td class="buttuom1">{!! $value->orderdetail_product->name !!}</td>
                                        <td class="buttuom1">
                                            <img style="width: 100px"
                                                 src="{!! asset('image/'.$value->orderdetail_product->image) !!}"
                                                 alt="">
                                        <td class="buttuom1">{!! $value->quanlity !!}</td>
                                        <td class="buttuom1">{!! number_format($value->price,0) !!}đ</td>
                                        <td class="buttuom1">{!! number_format($value->price * $value->quanlity,0) !!}
                                            đ
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="row">
                                <h1>Tổng Thành Tiền : {!! number_format($total,0) !!}đ</h1>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection