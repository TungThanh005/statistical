@extends('backend.index')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @if(session('thongbao'))
                                <div class="alert alert-success">{!! session('thongbao') !!}</div>
                            @endif
                            <h2 class="custom">
                                Dách Sách Mua hàng
                            </h2>
                        </div>
                        <div class="body table-responsive ">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên</th>
                                    <th>email</th>
                                    <th>địa chỉ</th>
                                    <th>Số điện thoại</th>
                                    <th>Tổng tiền</th>
                                    <th>Trạng thái</th>
                                    <th>Ngày đặt hàng</th>
                                    <th>Thao tác</th>
                                    <th>xem chi tiết</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($order as $key => $value)
                                    <tr class="">
                                        <th class="buttuom"
                                            scope="row">{!! ((($order->currentPage()*10)-10)+1)+$key !!}</th>
                                        <td class="buttuom1">{!! $value->name !!}</td>
                                        <td style="width: 40px;" class="buttuom">{!! $value->email !!}</td>
                                        <td class="buttuom1">{!! $value->address !!}</td>
                                        <td class="buttuom">{!! $value->phone !!}</td>
                                        <td class="buttuom1">
                                            <?php
                                            $total = 0;
                                            foreach ($value->order_orderdetail as $values) {
                                                $total += $values['price'] * $values['quanlity'];
                                            }
                                            echo number_format($total, 0) . 'đ';
                                            ?>
                                        </td>
                                        <td class="buttuom">
                                            @if($value->status ==1)
                                                {!! "đặt hàng" !!}
                                            @else
                                                {!! 'Đóng Đơn' !!}
                                            @endif
                                        </td>
                                        <td class="buttuom1">
                                            {!! $value->created_at !!}
                                        </td>
                                        <td>
                                            @if($value->status==1)
                                                <a href="{!! route('ConfirmOrder',['id'=>$value->id]) !!}"
                                                   id="confirmcheckout"
                                                   class="btn btn-warning glyphicon glyphicon-repeat"
                                                   title="Xác nhận đơn hàng"></a>
                                            @else
                                                <span id="notificationcheckout" class="glyphicon glyphicon-ok"
                                                      title="Đơn hàng này đã được giao"></span>
                                            @endif
                                        </td>
                                        <td><a id="detailcheckout"
                                               class="btn btn-primary glyphicon glyphicon-step-forward"
                                               title="Xem chi tiết đơn hàng"
                                               href="{!! route('detailOrder',['id'=>$value->id]) !!}"></a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {!! $order->links() !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection