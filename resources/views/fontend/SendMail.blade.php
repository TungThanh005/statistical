<table style="text-align: center" border="1px solide " width="600px">
    <tr>
        <th>STT</th>
        <th>name</th>
        <th>Giá</th>
        <th>Số Lượng</th>
        <th>Ảnh</th>
        <th>thành tiền</th>
    </tr>
    @php $i=0 @endphp
    @php $total=0 @endphp
    @foreach($list as $key => $value)
        @php $i++ @endphp

        <tr>
            <td>{!! $i !!}</td>
            <td>{!! $value->name !!}</td>
            <td>{!! number_format($value->price,0,",",".") !!}</td>
            <td>{!! $value->quantity !!}</td>
            @php $images= $value->attributes->image @endphp
            @php $pathToFile= 'image/'.$images @endphp
            <td><img style="width: 70px;" src="{!! $message->embed($pathToFile) !!} "></td>
            <td>{!! number_format($value->price * $value->quantity,0,",",".") !!}</td>
            @php $total+= $value->price * $value->quantity @endphp
        </tr>
    @endforeach
</table>
<h1>Tổng Tiền :{!! number_format($total,0,",",".") !!} đ</h1>
