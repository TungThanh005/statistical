@extends('fontend.master')
@section('content')
    @include('fontend.slide')
    <div class="w3l_banner_nav_right">
        <div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_sub">
            @if(isset($newAllPro))
                <h3>Top Sản Phẩm Mới Nhất</h3>
                <div class="w3ls_w3l_banner_nav_right_grid1">
                    @foreach($newAllPro as $value)
                        <div class="col-md-3 w3ls_w3l_banner_left">
                            <div class="hover14 column">
                                <div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
                                    <div class="agile_top_brand_left_grid_pos">
                                        @if(!is_null($value->product_sale) && CheckSale($value->product_sale->start_at,getdate(),$value->product_sale->end_at)==true)
                                            <div class="tag"><img src="{!! asset('fonend/images/offer.png') !!}" alt=" "
                                                                  class="responsive"/></div>
                                        @endif
                                    </div>
                                    <div class="agile_top_brand_left_grid1">
                                        <figure>
                                            <div class="snipcart-item block">
                                                <div class="snipcart-thumb">
                                                    <a href="{!! route('DetailsPro',['id'=>$value->id]) !!}"><img
                                                                src="{!! asset('image/'.$value->image) !!}" alt=" "
                                                                class=""/></a>
                                                    <p>{!! $value->name !!}</p>
                                                    @if(!is_null($value->product_sale) && CheckSale($value->product_sale->start_at,getdate(),$value->product_sale->end_at)==true)
                                                        <h4>
                                                            Giá: {!! number_format($value->price - (($value->price * $value->product_sale->dis_count) / 100),0) !!}
                                                            đ
                                                            <span style="text-decoration: none;">  Sale: {!! $value->product_sale->dis_count!!}
                                                                %</span></h4>
                                                    @else
                                                        <h4>Giá:{!! number_format($value->price,0) !!}đ<span></span>
                                                        </h4>
                                                    @endif
                                                </div>
                                                <div class="snipcart-details top_brand_home_details">
                                                    <input type="submit" name="submit" value="Add to cart"
                                                           class="button" attr="{!! $value->id !!}"/>
                                                </div>
                                            </div>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="clearfix"></div>
                    {!! $newAllPro->links() !!}
                </div>
            @elseif(isset($proBuys))
                <h3>Top Sản Phẩm Bán Chạy Nhất</h3>
                <div style="height: 100%">
                    <div class="w3ls_w3l_banner_nav_right_grid1">
                        @foreach($proBuys as $value)
                            <div class="col-md-3 w3ls_w3l_banner_left">
                                <div class="hover14 column">
                                    <div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
                                        <div class="agile_top_brand_left_grid_pos">
                                            @if(!is_null($value->product_sale) && CheckSale($value->product_sale->start_at,getdate(),$value->product_sale->end_at)==true)
                                                <div class="tag"><img src="{!! asset('fonend/images/offer.png') !!}"
                                                                      alt=" " class="responsive"/></div>
                                            @endif
                                        </div>
                                        <div class="agile_top_brand_left_grid1">
                                            <figure>
                                                <div class="snipcart-item block">
                                                    <div class="snipcart-thumb">
                                                        <a href="{!! route('DetailsPro',['id'=>$value->id]) !!}"><img
                                                                    src="{!! asset('image/'.$value->image) !!}" alt=" "
                                                                    class=""/></a>
                                                        <p>{!! $value->name !!}</p>
                                                        @if(!is_null($value->product_sale) && CheckSale($value->product_sale->start_at,getdate(),$value->product_sale->end_at)==true)
                                                            <h4>
                                                                Giá: {!! number_format($value->price - (($value->price * $value->product_sale->dis_count) / 100),0) !!}
                                                                đ
                                                                <span style="text-decoration: none;">  Sale: {!! $value->product_sale->dis_count!!}
                                                                    %</span></h4>
                                                        @else
                                                            <h4>Giá:{!! number_format($value->price,0) !!}đ<span></span>
                                                            </h4>
                                                        @endif
                                                    </div>
                                                    <div class="snipcart-details top_brand_home_details">
                                                        <input type="submit" name="submit" value="Add to cart"
                                                               class="button" attr="{!! $value->id !!}"/>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="clearfix"></div>
                    {!! $proBuys->links() !!}
                </div>
            @elseif(isset($allViewPro))
                <h3>Top Sản Phẩm nhiều lượt xem nhất</h3>
                <div class="w3ls_w3l_banner_nav_right_grid1">

                    @foreach($allViewPro as $value)

                        <div class="col-md-3 w3ls_w3l_banner_left">
                            <div class="hover14 column">
                                <div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
                                    <div class="agile_top_brand_left_grid_pos">
                                        @if(!is_null($value->product_sale) && CheckSale($value->product_sale->start_at,getdate(),$value->product_sale->end_at)==true)
                                            <div class="tag"><img src="{!! asset('fonend/images/offer.png') !!}" alt=" "
                                                                  class="responsive"/></div>
                                        @endif
                                    </div>
                                    <div class="agile_top_brand_left_grid1">
                                        <figure>
                                            <div class="snipcart-item block">
                                                <div class="snipcart-thumb">
                                                    <a href="{!! route('DetailsPro',['id'=>$value->id]) !!}"><img
                                                                src="{!! asset('image/'.$value->image) !!}" alt=" "
                                                                class=""/></a>
                                                    <p>{!! $value->name !!}</p>
                                                    @if(!is_null($value->product_sale) && CheckSale($value->product_sale->start_at,getdate(),$value->product_sale->end_at)==true)
                                                        <h4>
                                                            Giá: {!! number_format($value->price - (($value->price * $value->product_sale->dis_count) / 100),0) !!}
                                                            đ
                                                            <span style="text-decoration: none;">  Sale: {!! $value->product_sale->dis_count!!}
                                                                %</span></h4>
                                                    @else
                                                        <h4>Giá:{!! number_format($value->price,0) !!}đ<span></span>
                                                        </h4>
                                                    @endif
                                                </div>
                                                <div class="snipcart-details top_brand_home_details">
                                                    <input type="submit" name="submit" value="Add to cart"
                                                           class="button" attr="{!! $value->id !!}"/>
                                                </div>
                                            </div>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="clearfix"></div>
                    {!! $allViewPro->links() !!}
                </div>
            @else
                <h3 style="text-align:left">+Danh mục : {!! $listcate->listcate_cate->name !!}/{!! $listcate->name !!}
                    :</h3>
                <div class="w3ls_w3l_banner_nav_right_grid1">

                    @foreach($data as $value)

                        <div class="col-md-3 w3ls_w3l_banner_left">
                            <div class="hover14 column">
                                <div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
                                    <div class="agile_top_brand_left_grid_pos">
                                        @if(!is_null($value->product_sale) && CheckSale($value->product_sale->start_at,getdate(),$value->product_sale->end_at)==true)
                                            <div class="tag"><img src="{!! asset('fonend/images/offer.png') !!}" alt=" "
                                                                  class="responsive"/></div>
                                        @endif
                                    </div>
                                    <div class="agile_top_brand_left_grid1">
                                        <figure>
                                            <div class="snipcart-item block">
                                                <div class="snipcart-thumb">
                                                    <a href="{!! route('DetailsPro',['id'=>$value->id]) !!}"><img
                                                                src="{!! asset('image/'.$value->image) !!}" alt=" "
                                                                class=""/></a>
                                                    <p>{!! $value->name !!}</p>
                                                    @if(!is_null($value->product_sale) && CheckSale($value->product_sale->start_at,getdate(),$value->product_sale->end_at)==true)
                                                        <h4>
                                                            Giá: {!! number_format($value->price - (($value->price * $value->product_sale->dis_count) / 100),0) !!}
                                                            đ
                                                            <span style="text-decoration: none;">  Sale: {!! $value->product_sale->dis_count!!}
                                                                %</span></h4>
                                                    @else
                                                        <h4>Giá:{!! number_format($value->price,0) !!}đ<span></span>
                                                        </h4>
                                                    @endif
                                                </div>
                                                <div class="snipcart-details top_brand_home_details">
                                                    <input type="submit" name="submit" value="Add to cart"
                                                           class="button" attr="{!! $value->id !!}"/>
                                                </div>
                                            </div>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="clearfix"></div>
                    {!! $data -> links() !!}
                </div>
            @endif
        </div>
    </div>
    <div class="clearfix"></div>
@endsection