@extends('fontend.master')
@section('content')
@include('fontend.slide')
<div class="top-brands">
    <div class="container">
        <h3>Kết Quả Cho Tìm Kiềm : <span style="color: #4c110f;text-decoration: underline">{!! $key !!}</span></h3>
        <div class="agile_top_brands_grids">
            @foreach($search as $value)
            <div class="col-md-3 top_brand_left">
                <div class="hover14 column">
                    <div class="agile_top_brand_left_grid">
                        <div class="tag"><img src="{!! asset('fonend/images/tag.png') !!}" alt=" "
                                              class="img-responsive"/></div>
                        <div class="agile_top_brand_left_grid1">
                            <figure>
                                <div class="snipcart-item block">
                                    <div class="snipcart-thumb">
                                        <a href="{!! route(" DetailsPro",["id"=>$value->id]) !!}"><img title=" " alt=" "
                                                                                                       src="{!! asset('image/'.$value->image) !!}"/></a>
                                        <p>{!! $value->name !!}</p>
                                        <h4>{!! number_format($value->price,0) !!}&nbsp;vnđ<span>$10.00</span></h4>
                                    </div>
                                    <div class="snipcart-details top_brand_home_details">
                                        <input type="submit" attr="{!! $value->id !!}" name="submit" value="Add to cart"
                                               class="button button1" id="button"/>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endsection