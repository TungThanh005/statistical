<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
    <title>Grocery Store a Ecommerce Online Shopping Category Flat Bootstrap Responsive Website Template | Home ::
        w3layouts</title>
    <!-- for-mobile-apps -->
    {{--<meta name="viewport" content="width=device-width, initial-scale=1">--}}
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Grocery Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{--<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //for-mobile-apps -->--}}
    <link href="{!! asset('fonend/css/bootstrap.css') !!}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{!! asset('fonend/css/style.css') !!}" rel="stylesheet" type="text/css" media="all"/>
    <!-- font-awesome icons -->
    {{--<link href="{!! asset('fonend/css/font-awesome.css') !!}" rel="stylesheet" type="text/css" media="all" />--}}
    <link href="{!! asset('backend/css/my.css') !!}" rel="stylesheet" type="text/css" media="all"/>
    <!-- //font-awesome icons -->
    <!-- js -->
    <script src="{!! asset('fonend/js/jquery-1.11.1.min.js') !!}"></script>
    <link rel="stylesheet" href="{!! asset('backend/jquery-ui-1.12.1/jquery-ui.css') !!}">
    <script src="{!! asset('backend/jquery-ui-1.12.1/jquery-ui.js') !!}"></script>

    <!-- //js -->

    <!-- start-smoth-scrolling -->
    <script type="text/javascript" src="{!! asset('fonend/js/move-top.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('fonend/js/easing.js') !!}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
            });
        });
    </script>
    <!-- start-smoth-scrolling -->
</head>

<body>
@include('fontend.header')
@include('fontend.sidebar')
@yield('content')
@include('fontend.footer')



<!-- Bootstrap Core JavaScript -->
<script src="{!! asset('fonend/js/bootstrap.min.js') !!}"></script>
<script>
    $(document).ready(function () {
        $(".dropdown").hover(
            function () {
                $('.dropdown-menu', this).stop(true, true).slideDown("fast");
                $(this).toggleClass('open');
            },
            function () {
                $('.dropdown-menu', this).stop(true, true).slideUp("fast");
                $(this).toggleClass('open');
            }
        );
    });
</script>
<!-- here stars scrolling icon -->
{{--<script type="text/javascript">
    $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
            };
        */

        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>--}}
<!-- //here ends scrolling icon -->

{{--<script src="{!! asset('fonend/js/minicart.js') !!}"></script>--}}
<script src="{!! asset('backend/js/my.js') !!}"></script>
<script src="{!! asset('backend/js/sweetalert.min.js') !!}"></script>
<script src="{!! asset('backend/js/jquery.validate.min.js') !!}"></script>

</body>
</html>
