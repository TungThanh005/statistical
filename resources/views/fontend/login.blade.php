@extends('fontend.master')
@section('content')
    <div id="page-wrapper">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4 col-md-offset-3">
                        @if(Session::has('thongbao'))
                            <div STYLE="text-align: center ; height: 50px"
                                 class="alert alert-success">{!! Session::get('thongbao') !!}</div>
                        @endif
                        <h1>Đăng Nhập Vào Website</h1>
                        <form role="form" action="{!! route('postLogin') !!}" method="post">
                            @csrf
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" value="{!! old('email') !!}" placeholder="E-mail"
                                           id="email" name="email" type="text" autofocus>
                                    @if($errors->has('email'))
                                        <div class="text-danger">{!! $errors->first('email') !!}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" id="password"
                                           type="password" value="">
                                    <img id="showpass" class="showpass"
                                         style="float:right; margin-top:-29px;margin-right: 12px;"
                                         src="{!! asset('/image/showpass.png') !!}" alt="">
                                    @if($errors->has('password'))
                                        <div class="text-danger">{!! $errors->first('password') !!}
                                        </div>
                                    @endif
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox">Remember Me
                                    </label>
                                </div>
                                <input id="btnsubmit" class="btn btn-lg btn-success btn-block" type="submit"
                                       name="submit" value="Đăng Nhập">
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection