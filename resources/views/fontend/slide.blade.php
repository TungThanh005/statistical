<div class="w3l_banner_nav_right">
    <section class="slider">
        <div class="flexslider">
            <ul class="slides">
                @foreach($allslide as $value)
                    <li>
                        <div class="w3l_banner_nav_right_banner"
                             style="background:url({!! asset('image/'.$value->image) !!}) no-repeat 0px 0px;">
                            {{--<img src="{!! asset('fonend/images/1.jpg') !!}" alt="">--}}
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>
    <!-- flexSlider -->
    <link rel="stylesheet" href="{!! asset('fonend/css/flexslider.css') !!}" type="text/css" media="screen"
          property=""/>
    <script defer src="{!! asset('fonend/js/jquery.flexslider.js') !!}"></script>
    <script type="text/javascript">
        $(window).load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                start: function (slider) {
                    $('body').removeClass('loading');
                }
            });
        });
    </script>
    <!-- //flexSlider -->
</div>
<div class="clearfix"></div>