@extends('fontend.master')
@section('content')
    @include('fontend.slide')
    <div class="privacy about">
        <h3>Lịch Sử Mua Hàng Của Bạn</h3>

        <div class="checkout-right">
            <table class="timetable_sub">
                <thead>
                <tr>
                    <th>Stt</th>
                    <th>Tên Sản Phẩn</th>
                    <th>Hình Ảnh</th>
                    <th>Số lượng</th>
                    <th>Giá Tiền</th>
                    <th>Thành Tiền</th>
                    <th>Tổng Tiền</th>
                    <th>Ngày Mua</th>
                    <th>Thao tác</th>
                </tr>
                </thead>
                <tbody>
                @foreach($history as $key => $value)
                    <tr class="rem1">
                        <td class="invert">{!! ((($history->currentPage()*10)-10)+1)+$key !!}</td>
                        <td class="invert">
                            @foreach($value->order_orderdetail as $values)
                                @foreach($pros as $pro)
                                    @if($pro->id == $values->product_id)
                                        {!! $pro->name !!}<br><br><br>
                                    @endif
                                @endforeach
                            @endforeach

                        </td>
                        <td class="invert-image">
                            @foreach($value->order_orderdetail as $values)
                                @foreach($pros as $pro)
                                    @if($pro->id == $values->product_id)
                                        <img style="width: 50px" src="{!! asset('image/'.$pro->image) !!}" alt=""><br>
                                    @endif
                                @endforeach
                            @endforeach
                        </td>
                        <td class="invert">
                            @foreach($value->order_orderdetail as $values)
                                {!! $values->quanlity !!}<br><br><br>
                            @endforeach

                        </td>
                        <td class="invert">
                            @foreach($value->order_orderdetail as $values)
                                {!! number_format($values->price,0) !!} <br><br><br>
                            @endforeach
                        </td>
                        <td class="invert" id="{!! $value->id !!}">
                            @foreach($value->order_orderdetail as $values)
                                {!! number_format($values->price * $values->quanlity,0) !!}<br><br><br>
                            @endforeach
                        </td>
                        <td>
                            <?php
                            $total = 0;
                            foreach ($value->order_orderdetail as $values)
                                $total += $values->price * $values->quanlity;
                            echo number_format($total, 0);
                            ?>

                        </td>
                        <td class="invert">{!! $value->created_at !!}</td>
                        <td style="width: 227px" class="invert">
                            @if($value->status ==1)
                                <div class="rem deletehistory" attr="{!! $value->id !!}">
                                    <div style="width: 62px;right: 55px;" class="close1"><a href=""></a></div>
                                    @else
                                        <span class="btn btn-success">Đã Chốt Đơn Hàng</span>
                                    @endif
                                </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $history->links() !!}
        </div>
    </div>
@endsection
