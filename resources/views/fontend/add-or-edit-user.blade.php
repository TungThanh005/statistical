@extends('fontend.master')
@section('content')
    <div id="page-wrapper">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-5 col-md-offset-3">
                        @if(isset($user))
                            <h1>Thay đổi thông tin tài khoản</h1>
                            <form role="form" action="{!! route('PostAddOrEditUserFontend',['id'=>$user->id]) !!}"
                                  method="post" enctype="multipart/form-data">
                                @else
                                    <h1>Nhập Thông tin Đăng Kí</h1>
                                    <form role="form" action="{!! route('PostAddOrEditUserFontend') !!}" method="post"
                                          enctype="multipart/form-data">
                                        @endif
                                        @csrf
                                        <div class="form-group">
                                            <label for="disabledSelect">Name</label>
                                            <input class="form-control" type="text" id="name" name="name"
                                                   @if(isset($user)) value="{!! $user->name !!}"
                                                   @else value="{!! old('name') !!}" @endif>
                                        </div>
                                        @if($errors ->has('name'))
                                            <span class="text-danger">{!! $errors->first('name') !!}</span>
                                        @endif
                                        <div class="form-group">
                                            <label for="disabledSelect">Email</label>
                                            <input class="form-control" type="text" id="email" name="email"
                                                   @if(isset($user)) value="{!! $user->email !!}"
                                                   @else value="{!! old('email') !!}" @endif>
                                            <span id="flase" class="text-danger"></span>
                                            <span id="true" style="color:#1ab7ea;" class="success"></span>

                                            @if($errors ->has('email'))
                                                <span class="text-danger">{!! $errors->first('email') !!}</span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="disabledSelect">Password</label>
                                            <input class="form-control" type="password" id="password" name="password"
                                                   placeholder="Nhập tên password muốn thêm">
                                            @if($errors ->has('password'))
                                                <span class="text-danger">{!! $errors->first('password') !!}</span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="disabledSelect">Nhâp lại Password</label>
                                            <input class="form-control" type="password" id="passwordold"
                                                   name="passwordold"
                                                   placeholder="Nhập lại password ">
                                            @if($errors ->has('passwordold'))
                                                <span class="text-danger">{!! $errors->first('passwordold') !!}</span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="disabledSelect">Address</label>
                                            <input class="form-control" type="text" id="address" name="address"
                                                   @if(isset($user)) value="{!! $user->address !!}"
                                                   @else value="{!! old('address') !!}" @endif>
                                            @if($errors ->has('address'))
                                                <span class="text-danger">{!! $errors->first('address') !!}</span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="disabledSelect">Phone</label>
                                            <input class="form-control" type="text" id="phone" name="phone"
                                                   @if(isset($user)) value="{!! $user->phone !!}"
                                                   @else value="{!! old('phone') !!}" @endif>
                                            @if($errors ->has('phone'))
                                                <span class="text-danger">{!! $errors->first('phone') !!}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="disabledSelect">Chọn Ngày Sinh Của Bạn</label>
                                            <input class="form-control" type="date" id="birthday" name="birthday"
                                                   @if(isset($user)) value="{!! $user->birthday !!}"
                                                   @else value="{!! old('birthday') !!}" @endif>
                                            @if($errors ->has('birthday'))
                                                <span class="text-danger">{!! $errors->first('birthday') !!}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label class="">+Chọn giới tính</label><br>
                                            <input type="radio" name="gender" id="male" class="with-gap" value="0"
                                            @if(isset($user))
                                                @if($user->gender==0)
                                                    {!! "checked" !!}
                                                        @endif
                                                    @elseif(old('gender')==0)
                                                {!! "checked" !!}
                                                    @endif >
                                            <label for="male">Nam</label>
                                            <input type="radio" name="gender" id="female" class="with-gap" value="1"
                                            @if(isset($user))
                                                @if($user->gender==1)
                                                    {!! "checked" !!}
                                                        @endif
                                                    @elseif(old('gender')==1)
                                                {!! "checked" !!}
                                                    @endif >
                                            <label for="female" class="m-l-20">Nữ</label>
                                        </div>
                                        @if($errors->has('gender'))
                                            <div class="alert alert-danger">{!! $errors->first('gender') !!}</div>
                                        @endif
                                        <div class="form-group">
                                            <label for="disabledSelect">Avatar</label>
                                            <input type="file" name="avatar">
                                            @if($errors->has('avatar'))
                                                <div>
                                                    <span class="text-danger">{!! $errors->first('avatar') !!}</span>
                                                </div>
                                            @endif
                                            @if(isset($user))
                                                <img src="{!! asset('image/'.$user->avatar) !!}" alt="">
                                            @endif
                                        </div>
                                        @if(isset($user))
                                            <button type="submit" id="submit" class="btn btn-primary">Thay đổi thông
                                                tin
                                            </button>
                                        @else
                                            <button type="submit" id="submit" class="btn btn-primary">Đăng kí thông
                                                tin
                                            </button>
                                        @endif
                                    </form>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection