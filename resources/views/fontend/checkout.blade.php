@extends('fontend.master')
@section('content')
    @include('fontend.slide')
    <div class="privacy about">
        <h3>Thông Tin Chi Tiết Rỏ Hàng</h3>

        <div class="checkout-right">
            @if(!is_null($product))
                <table class="timetable_sub">
                    <thead>
                    <tr>
                        <th>Hình Ảnh</th>
                        <th>Số lượng</th>
                        <th>Tên Sản Phẩn</th>
                        <th>Giá Tiền</th>
                        <th>Tổng Tiền</th>
                        <th>Thao tác</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $total=0; @endphp
                    @foreach($product as $key => $value)
                        <tr class="rem1 {!! $value->id !!}">
                            <td class="invert-image">
                                <img style="width: 100px" src="{!! asset('image/'.$value->attributes->image) !!}"
                                     alt=" " class="img-responsive">
                            </td>
                            <td class="invert">
                                <div class="quantity">
                                    <div class="quantity-select">
                                        <div id="t{!! $value->id !!}" data-val="{!! $value->id !!}"
                                             attr="{!! $value->id !!}" class="entry value-minus">&nbsp;
                                        </div>
                                        <div id="h{!! $value->id !!}"
                                             class="entry value checkload">{!! $value->quantity !!}</div>
                                        <div id="d{!! $value->id !!}" attr="{!! $value->id !!}"
                                             class="entry value-plus active">&nbsp;
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="invert">{!! $value->name !!}</td>
                            <td class="invert"> {!! number_format($value->price,0) !!}</td>
                            <td class="invert"
                                id="{!! $value->id !!}">{!! number_format($value->price * $value->quantity,0) !!}</td>
                            <td class="invert">
                                <div class="rem " attr="{!! $value->id !!}">
                                    <div style="width: 62px;" class="close1"></div>
                                </div>
                            </td>
                        </tr>
                        @php $total+= $value->price * $value->quantity @endphp
                    @endforeach
                    </tbody>
                </table>
                <h1 id="updateprice">Tổng Tiền Của Đơn Hàng= {!! number_format($total,0) !!}đ</h1>
            @endif
        </div>
        <div class="checkout-left">
            <div class="col-md-4 checkout-left-basket">
                <h4>Continue to basket</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur cumque dolores ducimus eum
                    eveniet expedita fugiat id iste laudantium minus necessitatibus nesciunt omnis perspiciatis quia rem
                    ut, veniam, veritatis vero.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur cumque dolores ducimus eum
                    eveniet expedita fugiat id iste laudantium minus necessitatibus nesciunt omnis perspiciatis quia rem
                    ut, veniam, veritatis vero.</p>
            </div>
            <div class="col-md-8 address_form_agile">
                <h4>Thông Tin Thanh TOán</h4>
                <form action="{!! route('payment') !!}" method="post" class="creditly-card-form agileinfo_form">
                    @csrf
                    <section class="creditly-wrapper wthree, w3_agileits_wrapper">
                        <div class="information-wrapper">
                            <div class="first-row form-group">
                                <div class="controls">
                                    <label class="control-label">Tên Của bạn: </label>
                                    <input class="billing-address-name form-control" type="text" name="name"
                                           value="{!! \Illuminate\Support\Facades\Auth::user()->name !!}">
                                </div>
                                @if($errors->has('name'))
                                    <div class="alert alert-danger">{!! $errors->first('name') !!}</div>
                                @endif
                                <div class="w3_agileits_card_number_grids">
                                    <div class="w3_agileits_card_number_grid_left">
                                        <div class="controls">
                                            <label class="control-label">Số Điện Thoại:</label>
                                            <input class="form-control" type="text" name="phone"
                                                   value="{!! \Illuminate\Support\Facades\Auth::user()->phone !!}">
                                        </div>
                                    </div>
                                    @if($errors->has('phone'))
                                        <div class="alert alert-danger">{!! $errors->first('phone') !!}</div>
                                    @endif
                                    <div class="w3_agileits_card_number_grid_right">
                                        <div class="controls">
                                            <label class="control-label">email </label>
                                            <input class="form-control" type="text" name="email"
                                                   value="{!! \Illuminate\Support\Facades\Auth::user()->email !!}">
                                        </div>
                                    </div>
                                    @if($errors->has('email'))
                                        <div class="alert alert-danger">{!! $errors->first('email') !!}</div>
                                    @endif
                                    <div class="clear"></div>
                                </div>
                                <div class="controls">
                                    <label class="control-label">Địa Chỉ Nhận Hàng</label>
                                    <input class="form-control" type="text" name="address"
                                           value="{!! \Illuminate\Support\Facades\Auth::user()->address !!}">
                                </div>
                                @if($errors->has('address'))
                                    <div class="alert alert-danger">{!! $errors->first('address') !!}</div>
                                @endif
                            </div>
                            <button class="submit check_out">Đặt Hàng</button>
                        </div>
                    </section>
                </form>
            </div>

            <div class="clearfix"></div>

        </div>

    </div>
@endsection