<!-- header -->
<div class="agileits_header">
    <div class="w3l_offers">
        <a href="{!! route('home') !!}">chào mừng đến với GROCERY</a>
    </div>
    <div class="w3l_search">
        <form action="{!! route('search') !!}" method="post">
            @csrf
            <input type="text" name="key" value="Search a product..." onfocus="this.value = '';"
                   onblur="if (this.value == '') {this.value = 'Search a product...';}" required="">
            <input type="submit" value=" ">
        </form>
    </div>
    @if(\Illuminate\Support\Facades\Auth::check())

        <div class="btn-group HideCheckOut ">
            <button style="background: url({!! asset('image/rohang.jpg') !!}) no-repeat" type="text"
                    class=" dropdown-toggle fixrohang showProduct" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rỏ Hàng Của bạn
            </button>
            <div class="dropdown-menu dropdown-menu-right showajax">
            </div>
        </div>
        <div class="w3l_header_right">
            <ul>
                <li class="dropdown profile_details_drop">
                    <a href="#" class="dropdown-toggle fixdn" data-toggle="dropdown">
                        <img id="imguser" src="{!! asset('image/'.\Illuminate\Support\Facades\Auth::user()->avatar) !!}"
                             alt="">
                        <span class="caret"></span>
                    </a>
                    <div class="mega-dropdown-menu">
                        <div class="w3ls_vegetables">
                            <ul class="dropdown-menu drp-mnu custom-login">
                                <li class="fixedit">
                                    <a href="{!! route('AddEditUserFontend',['id'=>\Illuminate\Support\Facades\Auth::user()->id]) !!}">Thay
                                        đổi thông tin</a></li>
                                <li>
                                    <a href="{!! route('history',['id'=>\Illuminate\Support\Facades\Auth::user()->id]) !!}">Lịch
                                        Sử Mua Hàng</a></li>
                                <li>
                                    @if(\Illuminate\Support\Facades\Auth::user()->lever==1)
                                        <a href="{!! route('listOder') !!}">Đến Trang Quản Trị</a></li>
                                @endif
                                <li>
                                    <a href="{!! route('logout') !!}">Đăng xuất</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    @else
        <div class="w3l_header_right1">
            <h2 class="fixlogin"><a href="{!! route('login') !!}">Đăng Nhập</a></h2>
        </div>
        <div class="w3l_header_right1 center">
            <h2 class="fixdangki"><a href="{!! route('AddUserFontend') !!}">Đăng kí</a></h2>
        </div>
    @endif
    <div class="clearfix"></div>
</div>
<!-- script-for sticky-nav -->
<script>
    $(document).ready(function () {
        var navoffeset = $(".agileits_header").offset().top;
        $(window).scroll(function () {
            var scrollpos = $(window).scrollTop();
            if (scrollpos >= navoffeset) {
                $(".agileits_header").addClass("fixed");
            } else {
                $(".agileits_header").removeClass("fixed");
            }
        });

    });
</script>
<!-- //script-for sticky-nav -->
<div class="logo_products">
    <div class="container">
        <div class="row">
            <div class="w3ls_logo_products_left">
                <h1><a href="{!! route('home') !!}"><span>Grocery</span> Store</a></h1>
            </div>
            <div class="w3ls_logo_products_left1">
                <ul class="special_items">
                    <li><a class="list-group-item list-group-item-info" href="{!! route('Event') !!}">Sự Kiện</a></li>
                    <li><a class="list-group-item list-group-item-success" href="{!! route('About') !!}">Giới Thiệu</a>
                    </li>
                    <li><a class="list-group-item list-group-item-warning" href="{!! route('Services') !!}">Dịch Vụ</a>
                    </li>
                </ul>
            </div>
            <div class="w3ls_logo_products_left1">
                <ul class="phone_email">
                    <li><i class="fa fa-phone" aria-hidden="true"></i>(+0123) 234 567</li>
                    <li><i class="fa fa-envelope-o customFa" aria-hidden="true"></i><a href="mailto:store@grocery.com">tungjvb@gmail.com</a>
                    </li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div>
    @if(session('thongbao'))
        <div class="alert-success alert thongbao">{!! session('thongbao') !!}</div>
    @endif
    @if(session('thongBao'))
        <div class="alert-danger alert thongbao">{!! session('thongBao') !!}</div>
    @endif
</div>
<!-- //header -->


