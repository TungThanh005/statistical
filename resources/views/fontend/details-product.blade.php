@extends('fontend.master')
@section('content')
    @include('fontend.slide')
    <div class="w3l_banner_nav_right">
        <div class="agileinfo_single">
            <h5>{!! $detailpro->name !!}</h5>
            <div class="col-md-4 agileinfo_single_left">
                <img id="example" src="{!! asset('image/'.$detailpro->image) !!}" alt=" " class="img-responsive"/>
            </div>
            <div class="col-md-8 agileinfo_single_right">
                <div class="w3agile_description">
                    <h4>Mô Tả Thêm Về Sản Phẩm :</h4>
                    <p>{!! $detailpro->description !!}.</p>
                </div>
                <div class="snipcart-item block">
                    <div class="snipcart-thumb agileinfo_single_right_snipcart">
                        @if(!is_null($detailpro->product_sale) && CheckSale($detailpro->product_sale->start_at,getdate(),$detailpro->product_sale->end_at)==true)
                            <h4>
                                Giá:{!! number_format($detailpro->price - (($detailpro->price * $detailpro->product_sale->dis_count) / 100),0) !!}
                                vnđ <span
                                        style="text-decoration: none;">  Sale: {!! $detailpro->product_sale->dis_count!!}
                                    %</span></h4>
                        @else
                            <h4>{!! number_format($detailpro->price,0) !!}vnđ <span></span></h4>
                        @endif
                    </div>
                    <form action="{!! route('addCartDetail') !!}" method="post" id="quantitynumber">
                        @csrf
                        <fieldset>
                            <div class="snipcart-thumb agileinfo_single_right_snipcart">
                                <lable>Số Lượng</lable>
                                <h4><input style="width: 69px;" type="text" name="quantity"></h4>
                            </div>
                            <div></div>
                            <div class="snipcart-details agileinfo_single_right_details">
                                <input type="hidden" name="id" value="{!! $detailpro->id !!}"/>
                                <input type="submit" name="submit" value="Add to cart" class="button"/>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
    </div>
    <div class="w3l_banner_nav_right">
        <div class="agileinfo_single">
            <h5 class="edit-magin">Comment</h5>
            <textarea id="comment" rows="4" style="width: 70%"></textarea>
            <div style="overflow: visible; width:70%">
                <span attr="{!! $detailpro->id !!}" id="submitcomments" class="btn btn-primary ">Send</span>
                <div class="update_conment">
                    @foreach($comment as $value)
                        <p id="namecomment"><h5 class="edit-magin">{!! $value->comment_user->name !!}:</h5></p>
                        <span id="contentconment">{!! $value->content !!}</span>
                        @if(!is_null(\Illuminate\Support\Facades\Auth::user()))
                            @if(\Illuminate\Support\Facades\Auth::user()->lever ==1 || $value->user_id == \Illuminate\Support\Facades\Auth::user()->id)
                                <a style="float: right"
                                   href="{!! route('deleteComment',['id'=>$value->id]) !!}">delete</a>
                            @endif
                        @endif
                        <hr>
                    @endforeach
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
    </div>
    <!-- //banner -->
    <!-- brands -->
    <div class="top-brands">
        <div class="container">
            <h3>Sản Phẩm Liên Quan</h3>
            <div class="agile_top_brands_grids">
                @foreach($ProductMatch as $value)
                    <div class="col-md-3 top_brand_left">
                        <div class="hover14 column">
                            <div class="agile_top_brand_left_grid">
                                @if(!is_null($value->product_sale) && CheckSale($value->product_sale->start_at,getdate(),$value->product_sale->end_at)==true)
                                    <div class="tag"><img src="{!! asset('fonend/images/offer.png') !!}" alt=" "
                                                          class="img-responsive"/></div>
                                @endif
                                <div class="agile_top_brand_left_grid1">
                                    <figure>
                                        <div class="snipcart-item block">
                                            <div class="snipcart-thumb">
                                                <a href="{!! route("DetailsPro",["id"=>$value->id]) !!}"><img title=" "
                                                                                                              alt=" "
                                                                                                              src="{!! asset('image/'.$value->image) !!}"/></a>
                                                <p>{!! $value->name !!}</p>
                                                @if(!is_null($value->product_sale) && CheckSale($value->product_sale->start_at,getdate(),$value->product_sale->end_at)==true)
                                                    <h4>
                                                        Giá:{!! number_format($value->price - (($value->price * $value->product_sale->dis_count) / 100),0) !!}
                                                        vnđ <span
                                                                style="text-decoration: none;">  Sale: {!! $value->product_sale->dis_count!!}
                                                            %</span></h4>
                                                @else
                                                    <h4>{!! number_format($value->price,0) !!}vnđ <span></span></h4>
                                                @endif
                                            </div>
                                            <div class="snipcart-details top_brand_home_details">
                                                <input type="submit" attr="{!! $value->id !!}" name="submit"
                                                       value="Add to cart" class="button button1" id="button"/>
                                            </div>
                                        </div>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- //brands -->
    <!-- newsletter -->
    <!-- //newsletter -->
    <!-- footer -->
@endsection