@extends('fontend.master')
@section('content')
    @include('fontend.slide')
    <div class="banner_bottom">
        <div class="wthree_banner_bottom_left_grid_sub">
        </div>
        <div class="wthree_banner_bottom_left_grid_sub1">
            <div class="col-md-4 wthree_banner_bottom_left">
                <div class="wthree_banner_bottom_left_grid">
                    <img src="{!! asset('fonend/images/4.jpg') !!}" alt=" " class="img-responsive"/>
                    <div class="wthree_banner_bottom_left_grid_pos">
                        <h4>Ưu đãi giảm giá <span>25%</span></h4>
                    </div>
                </div>
            </div>
            <div class="col-md-4 wthree_banner_bottom_left">
                <div class="wthree_banner_bottom_left_grid">
                    <img src="{!! asset('fonend/images/5.jpg') !!}" alt=" " class="img-responsive"/>
                    <div class="wthree_banner_btm_pos">
                        <h3>Giới Thiệu <span>Cửa hàng tốt nhất</span></h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4 wthree_banner_bottom_left">
                <div class="wthree_banner_bottom_left_grid">
                    <img src="{!! asset('fonend/images/6.jpg') !!}" alt=" " class="img-responsive"/>
                    <div class="wthree_banner_btm_pos1">
                        <h3>Tiết kiệm <span>lên tới</span> 10%</h3>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <!-- top-brands -->
    <div class="top-brands">
        <div class="container">
            <h3>Top Sản Phẩm Mới Nhất</h3>
            <div class="agile_top_brands_grids">
                @foreach($product as $value)
                    <div class="col-md-3 top_brand_left">
                        <div class="hover14 column">
                            <div class="agile_top_brand_left_grid haha">
                                @if(!is_null($value->product_sale) && CheckSale($value->product_sale->start_at,getdate(),$value->product_sale->end_at)==true)
                                    <div class="tag"><img src="{!! asset('fonend/images/offer.png') !!}" alt=" "
                                                          class="img-responsive"/></div>
                                @endif
                                <div class="agile_top_brand_left_grid1">
                                    <figure>
                                        <div class="snipcart-item block">
                                            <div class="snipcart-thumb">
                                                <a href="{!! route("DetailsPro",["id"=>$value->id]) !!}"><img title=" "
                                                                                                              alt=" "
                                                                                                              src="{!! asset('image/'.$value->image) !!}"/></a>
                                                <p>{!! $value->name !!}</p>
                                                @if(!is_null($value->product_sale) && CheckSale($value->product_sale->start_at,getdate(),$value->product_sale->end_at)==true)
                                                    <h4>
                                                        Giá: {!! number_format($value->price - (($value->price * $value->product_sale->dis_count) / 100),0) !!}
                                                        đ
                                                        <span style="text-decoration: none;">  Sale: {!! $value->product_sale->dis_count!!}
                                                            %</span></h4>
                                                @else
                                                    <h4>Giá:{!! number_format($value->price,0) !!}đ <span></span></h4>
                                                @endif
                                            </div>
                                            <div class="snipcart-details top_brand_home_details">
                                                <input type="submit" attr="{!! $value->id !!}" name="submit"
                                                       value="Add to cart" class="button button1" id="button"/>
                                            </div>
                                        </div>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                @if(count($product) == 8)
                    <a class="btn btn-success" href="{!! route('allNewProduct') !!}">xem thêm</a>
                @endif
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- //top-brands -->
    <!-- fresh-vegetables -->
    <div class="fresh-vegetables">
        <div class="container">
            <div class="w3ls_w3l_banner_nav_right_grid1">
                <h6 style="text-align: center">Top Sản Phẩm Bán Chạy Nhất</h6>

                @foreach($products as $value)
                    <div class="col-md-3 w3ls_w3l_banner_left">
                        <div class="hover14 column">
                            <div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
                                <div class="agile_top_brand_left_grid_pos">
                                    {{--  @if(!is_null($value->product_sale) && CheckSale($value->product_sale->start_at,getdate(),$value->product_sale->end_at)==true)
                                          <div class="tag"><img src="{!! asset('fonend/images/offer.png') !!}" alt=" " class="img-responsive" /></div>
                                      @endif--}}
                                </div>
                                <div class="agile_top_brand_left_grid1">
                                    <figure>
                                        <div class="snipcart-item block">
                                            <div class="snipcart-thumb">
                                                <a href="{!! route('DetailsPro',['id'=>$value->id]) !!}"><img
                                                            src="{!! asset('image/'.$value->image) !!}" alt=" "
                                                            class=""/></a>
                                                <p>{!! $value->name !!}</p>
                                                @if(!is_null($value->product_sale) && CheckSale($value->product_sale->start_at,getdate(),$value->product_sale->end_at)==true)
                                                    <h4>
                                                        Giá:{!! number_format($value->price - (($value->price * $value->product_sale->dis_count) / 100),0) !!}
                                                        đ
                                                        <span style="text-decoration: none;">  Sale: {!! $value->product_sale->dis_count!!}
                                                            %</span></h4>
                                                @else
                                                    <h4>Giá:{!! number_format($value->price,0) !!}đ <span></span></h4>
                                                @endif
                                            </div>
                                            <div class="snipcart-details">
                                                <input type="submit" attr="{!! $value->id !!}" name="submit"
                                                       value="Add to cart" class="button"/>
                                            </div>
                                        </div>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                {{--@if(count($products) == 8)
                    <a class="btn btn-success" href="{!! route('proBuys) !!}">xem thêm</a>
                @endif--}}
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="top-brands">
        <div class="container">
            <h3>Top Sản Phẩm Nhiều Lượt Xem Nhất</h3>
            <div class="agile_top_brands_grids">
                @foreach($viewpro as $value)
                    <div class="col-md-3 top_brand_left">
                        <div class="hover14 column">
                            <div class="agile_top_brand_left_grid">
                                @if(!is_null($value->product_sale) && CheckSale($value->product_sale->start_at,getdate(),$value->product_sale->end_at)==true)
                                    <div class="tag"><img src="{!! asset('fonend/images/offer.png') !!}" alt=" "
                                                          class="img-responsive"/></div>
                                @endif
                                <div class="agile_top_brand_left_grid1">
                                    <figure>
                                        <div class="snipcart-item block">
                                            <div class="snipcart-thumb">
                                                <a href="{!! route("DetailsPro",["id"=>$value->id]) !!}"><img title=" "
                                                                                                              alt=" "
                                                                                                              src="{!! asset('image/'.$value->image) !!}"/></a>
                                                <p>{!! $value->name !!}</p>
                                                @if(!is_null($value->product_sale) && CheckSale($value->product_sale->start_at,getdate(),$value->product_sale->end_at)==true)
                                                    <h4>
                                                        Giá:{!! number_format($value->price - (($value->price * $value->product_sale->dis_count) / 100),0) !!}
                                                        đ
                                                        <span style="text-decoration: none;">  Sale: {!! $value->product_sale->dis_count!!}
                                                            %</span></h4>
                                                @else
                                                    <h4>Giá:{!! number_format($value->price,0) !!}đ <span></span></h4>
                                                @endif
                                            </div>
                                            <div class="snipcart-details top_brand_home_details">
                                                <input type="submit" attr="{!! $value->id !!}" name="submit"
                                                       value="Add to cart" class="button button1" id="button"/>
                                            </div>
                                        </div>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                {{--      @if(count($viewpro) == 8)
                          <a class="btn btn-success" href="{!! route('allView') !!}">xem thêm</a>
                      @endif--}}
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- //fresh-vegetables -->
@endsection