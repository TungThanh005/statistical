<?php

namespace App\Http\Middleware;

use Closure;
use Darryldecode\Cart\Facades\CartFacade;
use Illuminate\Support\Facades\Auth;

class CheckOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $cartTotalQuantity = CartFacade::session(Auth::user()->id)->getTotalQuantity();
        if ($cartTotalQuantity > 0) {
            return $next($request);
        } else
            return redirect()->route('home')->with('thongBao', 'Rỏ hàng của bạn chưa có sản phẩm nào');

    }
}
