<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class loginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|regex:/^[a-z][a-z0-9_\.]{2,32}@[a-z0-9\-]{3,}(\.[a-z]{2,4}){1,2}$/',
            'password' => 'required|regex:/^[A-Z][A-z0-9]{6,32}$/',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'email không được để trống',
            'email.regex' => 'bạn vui lòng nhập đúng đinh dạng email',
            'password.required' => 'Mật Khẩu không được để trống',
            'password.regex' => 'Mật khẩu có 6-32 kí tự , kí tự đầu tiên là chữ và phải viết hoa',

        ];
    }
}
