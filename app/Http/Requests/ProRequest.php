<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|min:2|max:100|unique:product,name,' . $this->id,
            'price' => 'required|numeric|min:4',
            'description' => 'required',
            'category' => 'required|exists:category,id',
            'listCategory_id' => 'required|exists:list_category,id',
            'status' => 'required|numeric',
        ];
        if ($this->id != 0) {
            if (!is_null($this->dis_count)) {
                $rules['start_at'] = "required";
                $rules['end_at'] = "required";
            }
            $rules['images'] = "image|mimes:jpeg,png,jpg,gif,svg,PNG,JPG,JPEG|max:5000";
            return $rules;
        } else {
            if (!is_null($this->dis_count)) {
                $rules['start_at'] = "required";
                $rules['end_at'] = "required";
            }
            $rules['images'] = "required|image|mimes:jpeg,png,jpg,gif,svg,PNG,JPG,JPEG|max:5000";
            return $rules;
        }
    }

    public function messages()
    {
        return [
            'required' => ':attribute không được để trống',
            'min' => ':attribute ít nhất phải có :min kí tự',
            'max' => ':attribute nhiều nhất là :max kí tự',
            'numeric' => 'bạn phải nhậ đúng định dạng số',
            'unique' => 'tên sản phẩm này đã tồn tại, vui lòng chọn tên khác',
            'mimes' => 'ảnh phải có đuôi jpeg,png,jpg,gif,svg,PNG,JPG hoặc JPEG',
            'image.max' => 'file ảnh tối đa là 5 megabyte',
            'image' => 'file đưa lên pahỉ à file ảnh',
            'exists' => 'không tìm thấy danh mục'
        ];
    }

    public function attributes()
    {
        return [
            'price' => 'giá',
            'description' => 'mô tả',
            'category' => 'danh mục',
            'listCategory_id' => 'danh mục',
            'images' => 'hình ảnh',
            'name' => 'Tên sản phẩm',
            'start_at' => 'Ngày Bắt đầu sale không được để trống vì bạn đã chọn giá sale',
            'end_at' => 'Ngày kết thức sale không được để trống vì bạn đã chọn giá sale',
        ];
    }
}
