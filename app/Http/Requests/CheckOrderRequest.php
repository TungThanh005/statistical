<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:50',
            'email' => 'required|regex:/^[a-z][a-z0-9_\.]{2,32}@[a-z0-9\-]{3,}(\.[a-z]{2,4}){1,2}$/',
            'passwordold' => 'same:password',
            'phone' => 'required|min:10|max:11,numeric',
            'address' => 'required|min:9',
        ];
    }

    public function messages()
    {
        return [
            'phone.required' => 'số điện thoại không được để trống',
            'email.required' => 'địa chỉ email không được để trống',
            'address.required' => 'địa chỉ không được để trống',
            'address.min' => 'địa chỉ ít nhất phải có 9 kí tự vui lòng kiểm tra lại địa chỉ của bạn',
            'name.required' => 'Tên không được để trống',
            'email.regex' => 'email phải ít nhất 6 kí tự nhiết nhất là 32 kí tự và bạn phải nhập đúng định dạng email',
            'name.min' => 'tên của bạn ít nhất phải có :min kí tự',
            'phone.min' => 'số điện thoại ít nhất phải có :min kí tự',
            'name.max' => 'tên của bạn nhiều nhất là :max kí tự',
            'phone.max' => 'số điẹn thoại nhiều nhất là :max kí tự',
        ];
    }
}
