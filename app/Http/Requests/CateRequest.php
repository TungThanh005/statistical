<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:50|unique:category,name,' . $this->id,
            'status' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute không được để trống',
            'min' => ':attribute đồ dài tối thiểu là :min kí tự',
            'max' => ':attribute đồ dài tối đa là :max kí tự',
            'unique' => 'tên danh mục đã tồn tại',
            'numeric' => 'nhập đúng định dạng số'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'tên danh mục',
            'status' => 'trạng thái'
        ];
    }
}
