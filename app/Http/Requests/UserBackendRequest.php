<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserBackendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return back();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->id != 0) {
            $rules = [
                'name' => 'required|min:2|max:50',
                'email' => 'required|regex:/^[a-z][a-z0-9_\.]{2,32}@[a-z0-9\-]{3,}(\.[a-z]{2,4}){1,2}$/|unique:users,email,' . $this->id,
                'passwordold' => 'same:password',
                'phone' => 'required|',
                'address' => 'required',
                'birthday' => 'required|',
                'avatar' => 'mimes:jpeg,png,jpg,gif,svg,PNG,JPG,JPEG|max:5000',
                'gender' => 'required'
            ];
            if (!is_null($this->password)) {
                $rules['password'] = "regex:/^[A-Z][A-z0-9]{6,32}$/";
            }
            return $rules;
        } else {
            return [
                'name' => 'required|min:2|max:50',
                'email' => 'required|regex:/^[a-z][a-z0-9_\.]{2,32}@[a-z0-9\-]{3,}(\.[a-z]{2,4}){1,2}$/|unique:users,email',
                'password' => 'required|regex:/^[A-Z][A-z0-9]{6,32}$/',
                'passwordold' => 'required|same:password',
                'phone' => 'required|min:10|max:11,numeric',
                'address' => 'required',
                'birthday' => 'required',
                'avatar' => 'required|mimes:jpeg,png,jpg,gif,svg,PNG,JPG,JPEG|max:5000',
                'gender' => 'required'
            ];
        }
    }

    public function messages()
    {
        return [
            'phone.required' => 'số điện thoại không được để trống',
            'email.required' => 'địa chỉ email không được để trống',
            'password.required' => 'mật khẩu không được để trống',
            'passwordold.required' => 'vui lòng nhập lại mật khẩu',
            'address.required' => 'địa chỉ không được để trống',
            'birthday.required' => ' Vui lòng chọn ngày sinh của bạn',
            'avatar.required' => 'ảnh địa diện không được để trống',
            'gender.required' => 'vui lòng chọn giới tình của bạn',
            'name.required' => 'Tên không được để trống',
            'email.regex' => 'email phải ít nhất 6 kí tự nhiết nhất là 32 kí tự và bạn phải nhập đúng định dạng email',
            'name.min' => 'tên của bạn ít nhất phải có :min kí tự',
            'phone.min' => 'số điện thoại ít nhất phải có :min kí tự',
            'name.max' => 'tên của bạn nhiều nhất là :max kí tự',
            'phone.max' => 'số điẹn thoại nhiều nhất là :max kí tự',
            'unique' => 'email đã tồn tại',
            'same' => 'nhập lại mật khẩu không khớp',
            'password.regex' => 'Mật khẩu viết hoa chữ cái đầu,đồ dài từ 6-32 kí tự',
            'avatar.max' => 'kích thước ảnh phải nhỏ hơn hoặc bằng 5mb',
            'mimes' => 'đuôi ảnh phải có dạng jpeg,png,jpg,gif,svg,PNG,JPG,JPEG',
            'numeric' => 'bạn phải nhập định dạng số'

        ];
    }
}
