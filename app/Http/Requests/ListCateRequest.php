<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ListCateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:50|unique:list_category,name,' . $this->id,
            'category_id' => 'required',
            'status' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên danh mục không được để trống',
            'name.min' => 'Tên danh mục ít nhất cần có 2 kí tự',
            'name.max' => 'tên danh mục nhiếu nhất là 50 kí tự',
            'category_id.required' => 'bạn phải chọn dnah mục tương ứng',
            'status.required' => 'xin mời chọn',
            'status.numeric' => 'trường phải là định dạng số',
            'name.unique' => 'tên này đã tồn tại'
        ];
    }
}
