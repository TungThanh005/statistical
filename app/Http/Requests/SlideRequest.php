<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SlideRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->id != 0) {
            return [
                'image' => 'image|mimes:jpeg,png,jpg,gif,svg,PNG,JPG,JPEG|max:5000|unique:slide,image,'
            ];
        } else {
            return [
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg,PNG,JPG,JPEG|max:5000|unique:slide,image,',
            ];
        }
    }

    public function messages()
    {
        return [
            'required' => ':attribute không được để trống',
            'mimes' => 'ảnh phải có đuôi jpeg,png,jpg,gif,svg,PNG,JPG hoặc JPEG',
            'image.max' => 'file ảnh tối đa là 5 megabyte',
            'image' => 'file đưa lên phải à file ảnh'
        ];
    }

    public function attributes()
    {
        return [
            'image' => 'ảnh',
        ];
    }
}
