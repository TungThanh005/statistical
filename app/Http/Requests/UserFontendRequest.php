<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserFontendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|min:2|max:50',
            'email' => 'required|email|unique:users,email,' . $this->id,
            'phone' => 'required|regex:/^[(+84)]{5,5} [0-9]{3,3}\.[0-9]{3,3}\.[0-9]{3,3}$/',
            'address' => 'required',
            'birthday' => 'required',
            'avatar' => 'mimes:jpeg,png,jpg,gif,svg,PNG,JPG,JPEG|max:5000',
            'gender' => 'required'
        ];
        if ($this->id != 0) {
            if (!is_null($this->password)) {
                $rules['password'] = "regex:/^[A-Z][A-z0-9]{6,32}$/";
            }
            $rules['passwordold'] = "same:password";
            $rules['avatar'] = "mimes:jpeg,png,jpg,gif,svg,PNG,JPG,JPEG|max:5000";
            return $rules;
        } else {
            $rules['passwordold'] = "required|same:password";
            $rules['password'] = "required|regex:/^[A-Z][A-z0-9]{6,32}$/";
            $rules['avatar'] = "required|mimes:jpeg,png,jpg,gif,svg,PNG,JPG,JPEG|max:5000";
            return $rules;
        }

    }

    public function messages()
    {
        return [
            'phone.regex' => 'hãy nhập theo định dang (+84) .xxx.xxx.xxx',
            'phone.required' => 'số điện thoại không được để trống',
            'email.required' => 'địa chỉ email không được để trống',
            'password.required' => 'mật khẩu không được để trống',
            'passwordold.required' => 'vui lòng nhập lại mật khẩu',
            'address.required' => 'địa chỉ không được để trống',
            'birthday.required' => 'ngày sinh không được để trống',
            'avatar.required' => 'ảnh địa diện không được để trống',
            'gender.required' => 'vui lòng chọn giới tình của bạn',
            'name.required' => 'Tên không được để trống',
            'email.email' => 'hãy nhập đúng định dang email',
            'name.min' => 'tên của bạn ít nhất phải có :min kí tự',
            'phone.min' => 'số điện thoại ít nhất phải có :min kí tự',
            'name.max' => 'tên của bạn nhiều nhất là :max kí tự',
            'phone.max' => 'số điẹn thoại nhiều nhất là :max kí tự',
            'unique' => 'email đã tồn tại',
            'same' => 'nhập lại mật khẩu không khớp',
            'password.regex' => 'Mật khẩu viết hoa chữ cái đầu,đồ dài từ 6-32 kí tự',
            'avatar.max' => 'kích thước ảnh phải nhỏ hơn hoặc bằng 5mb',
            'mimes' => 'đuôi ảnh phải có dạng jpeg,png,jpg,gif,svg,PNG,JPG,JPEG',
            'numeric' => 'bạn phải nhập định dạng số'

        ];
    }

}