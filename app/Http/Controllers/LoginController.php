<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login()
    {
        return view('fontend.login');
    }

    public function postLogin(LoginRequest $request)
    {

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

            if (Auth::user()->lever == 1) {
                return redirect()->route('ListUser')->with('thongbaoLogin', 'Đăng nhập thành công');
            } else {
                return redirect()->route('home');
            }
        } else {
            return back()->with('thongbao', 'Bạn đã nhập sai tên tài khoản hoặc mật khẩu');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');
    }

    public function checklogin()
    {
        // kiểm tra nếu user đăng nhập mới cho bình luận sản phẩm
        if (Auth::check()) {
            return response()->json([
                'errors' => true
            ]);
        } else {
            return response()->json([
                'errors' => false
            ]);
        }
    }

}
