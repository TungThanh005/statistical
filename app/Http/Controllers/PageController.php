<?php

namespace App\Http\Controllers;

use App\Events\DiscountEvent;
use App\Models\comment;
use App\Models\ListCategory;
use App\Models\Product;

class PageController extends Controller
{
    public function listProduct($id)
    {
        $data = Product::where('listCategory_id', $id)->paginate(12);
        $listcate = ListCategory::findOrFail($id);
        if (!is_null($data)) {
            return view('fontend.cate-pro', ['data' => $data, 'listcate' => $listcate]);
        } else
            abort(404);
    }

    public function DetailsPro($id)
    {
        $product = Product::findOrFail($id);
        $ProductMatch = Product::where('listCategory_id', $product->listCategory_id)->take(8)->get();
        $comment = comment::where('product_id', $product->id)->orderBy('created_at', 'DESC')->take(10)->get();
        event(new DiscountEvent($product));
        return view('fontend.details-product', ['detailpro' => $product, 'ProductMatch' => $ProductMatch, 'comment' => $comment]);
    }

    public function Event()
    {
        return view('fontend.event');
    }

    public function Services()
    {
        return view('fontend.services');
    }

    public function About()
    {
        return view('fontend.about');
    }


}
