<?php

namespace App\Http\Controllers;

use App\Http\Requests\CheckOrderRequest;
use App\Mail\OrderShipped;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\Sale;
use App\Models\User;
use Carbon\Carbon;
use Darryldecode\Cart\Facades\CartFacade;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class OrderController extends Controller
{
    public function listOder()
    {
        $order = Order::orderBy('status')
            ->with('order_orderdetail')
            ->orderByDesc('id')
            ->paginate(10);
        return view('backend.order.list-order', ['order' => $order]);
    }

    public function detailOrder($id)
    {
        $data = OrderDetail::where('order_id', $id)->get();
        $total = 0;
        foreach ($data as $value) {
            $total += $value->price * $value->quanlity;
        }
        return view('backend.order.detail-order', ['order' => $data, 'total' => $total]);
    }

    public function AddCart(Request $request)
    {
        $id = $request->id;
        $userId = Auth::user()->id;
        $product = Product::findOrFail($id);
        $name = $product->name;
        $sale = Sale::where('product_id', $id)->first();
        if ($sale && CheckSale($sale->start_at, getdate(), $sale->end_at) == true) {
            $price = $product->price - (($product->price * $sale->dis_count) / 100);
        } else {
            $price = $product->price;
        }
        CartFacade::session($userId)->add($id, $name, $price, 1, array('image' => $product->image));
        return "Thêm sản phẩm thành công";
    }

    public function ShowOrder()
    {
        $userId = Auth::user()->id;
        return CartFacade::session($userId)->getContent();
    }

    public function checkout()
    {
        $userId = Auth::user()->id;
        $i = 1;
        $product = CartFacade::session($userId)->getContent();
        return view('fontend.checkout', ['product' => $product, 'i' => $i]);
    }

    public function editqty(Request $request)
    {
        $userId = Auth::user()->id;
        $idpro = $request->ids;
        $qty = $request->qty;
        CartFacade::session($userId)->update($idpro, array(
            'quantity' => array(
                'relative' => false,
                'value' => $qty,
            )
        ));
        $item = CartFacade::session($userId)->get($idpro);
        $totalOrder = 0;
        $total = CartFacade::session($userId)->getContent();
        foreach ($total as $value) {
            $totalOrder += $value->price * $value->quantity;
        }
        return response()->json([
            'item' => $item,
            'total' => $totalOrder
        ]);
    }

    public function DeleteProCheckout(Request $request)
    {
        $id = $request->id;
        $userId = Auth::user()->id;
        CartFacade::session($userId)->remove($id);
        $totalOrder = 0;
        $total = CartFacade::session($userId)->getContent();
        foreach ($total as $value) {
            $totalOrder += $value->price * $value->quantity;
        }
        return response()->json(['errors' => false,
            'total' => $totalOrder]);
    }

    public function deleteHistory(Request $request)
    {
        $id = $request->id;
        $order = Order::findorFail($id);
        if ($order->status == 1) {
            $order_detail = OrderDetail::where('order_id', $id);
            $order_detail->delete();
            $order->delete();
            return response()->json(['errors' => false]);
        } else {
            return response()->json(['errors' => true]);
        }
    }

    public function ddCartDetail(Request $request)
    {
        $qty = $request->quantity;
        $id = $request->id;
        $userId = Auth::user()->id;
        $product = Product::findOrFail($id);
        $name = $product->name;
        $sale = Sale::where('product_id', $id)->first();
        if ($sale && CheckSale($sale->start_at, getdate(), $sale->end_at) == true) {
            $price = $product->price - (($product->price * $sale->dis_count) / 100);
        } else {
            $price = $product->price;
        }
        CartFacade::session($userId)->add($id, $name, $price, $qty, array('image' => $product->image));
        return redirect()->route('checkout');
    }

    public function payment(CheckOrderRequest $request)
    {
        $userId = Auth::user()->id;
        $user = Auth::user()->email;
        $data = $request->all();
        $order = new Order();
        $order->fill($data);
        $order->user_id = $userId;
        $order->save();
        $list = CartFacade::session($userId)->getContent();
        foreach ($list as $key => $value) {
            $orderdetail = new OrderDetail();
            $orderdetail->order_id = $order->id;
            $orderdetail->quanlity = $value->quantity;
            $orderdetail->price = $value->price;
            $orderdetail->product_id = $value->id;
            $orderdetail->save();
        };
        $when = Carbon::now()->addMinutes(100);
        Mail::to($request->email)
            ->later($when, new OrderShipped($list));
        CartFacade::session($userId)->clear($userId);
        return Redirect::route('history', $userId);
    }

    public function history($id)
    {
        $order = Order::where('user_id', $id)->orderByDesc('id')->paginate(10);
        $pro = DB::table('order')
            ->join('order_detail', 'order.id', '=', 'order_detail.order_id')
            ->join('product', 'product.id', '=', 'order_detail.product_id')
            ->select('product.id', 'product.image', 'product.name')->groupBy('product.id', 'product.image', 'product.name')
            ->where('order.user_id', $id)
            ->orderByDesc('order.created_at')
            ->get();
        return view('fontend.history', ['history' => $order, 'pros' => $pro]);
    }

    public function confirmOrder($id)
    {
        $order = Order::findOrFail($id);
        $order->status = 2;
        $order->save();
        return back();
    }

    public function allNewProduct()
    {
        $pro = Product::where('status', 1)->orderby('created_at', 'desc')->paginate(12);
        return view('fontend.cate-pro', ['newAllPro' => $pro]);
    }

    public function proBuys(Request $request)
    {
        $count = DB::select("SELECT product_id FROM `order_detail` GROUP BY product_id order BY SUM(quanlity) DESC ");
        $items = [];
        foreach ($count as $key => $value) {
            $items[] = Product::find($value->product_id);
        }
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($items);
        $perPage = 12;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedItems = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);
        $paginatedItems->setPath($request->url());
        return view('fontend.cate-pro', ['proBuys' => $paginatedItems]);
    }

    public function allView()
    {
        $allViewPro = Product::orderby('view', 'DESC')->paginate(12);
        return view('fontend.cate-pro', ['allViewPro' => $allViewPro]);
    }
}
