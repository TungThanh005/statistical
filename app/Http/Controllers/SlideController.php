<?php

namespace App\Http\Controllers;

use App\Http\Requests\SlideRequest;
use App\Models\Slide;
use Illuminate\Http\Request;

class SlideController extends Controller
{
    public function postAddSlide(SlideRequest $request)
    {
        $data = $request->all();
        $slide = new Slide();
        $slide->status = $request->status;
        $file = $request->image;
        $name = $file->getClientOriginalName();
        $path = public_path('image');
        $nameFull = time() . '-' . $name;
        $file->move($path, $nameFull);
        $slide->image = $nameFull;
        $slide->save();
        return redirect()->route('listSlide')->with('thongbao', 'thêm mới thành công');
    }

    public function listSlide()
    {
        $slide = Slide::paginate(10);
        return view('backend.slide.list-slideg', ['list' => $slide]);
    }

    public function addOrEditSlide($id = 0)
    {
        if ($id != 0) {
            $slide = Slide::findOrFail($id);
            return view('backend.slide.add-or-edit-slide', ['slide' => $slide]);
        } else {
            return view('backend.slide.add-or-edit-slide');
        }
    }

    public function postAddOrditSlide(SlideRequest $request, $id = 0)
    {
        $data = $request->all();

        if ($id != 0) {
            $slide = Slide::findOrFail($id);
        } else {
            $slide = new Slide();
        }
        $slide->fill($data);
        if (!is_null($request->image)) {
            $file = $request->image;
            if ($id != 0) {
                unlink('image/' . $slide->image);
            }
            $name = $file->getClientOriginalName();
            $path = public_path('image');
            $nameFull = time() . '-' . $name;
            $file->move($path, $nameFull);
            $slide->image = $nameFull;
        }
        $slide->save();
        if ($id != 0) {
            return redirect()->route('listSlide')->with('thongbao', 'sửa thành công');
        } else {
            return redirect()->route('listSlide')->with('thongbao', 'Thêm mới thành công');
        }
    }

    public function postDelSlide(Request $request)
    {
        $id = $request->id;
        $slide = Slide::findOrFail($id);
        unlink('image/' . $slide->image);
        $slide->delete();
        return response()->json([
            'errors' => true
        ]);
    }
}
