<?php

namespace App\Http\Controllers;

use App\Http\Requests\CateRequest;
use App\Models\Category;
use App\Models\ListCategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function addCate()
    {
        return view('backend.category.add-or-edit-cate');
    }

    public function listCate()
    {
        $cate = Category::paginate(10);
        return view('backend.category.list-cate', ['cate' => $cate]);
    }

    public function editCate($id)
    {
        $cate = Category::findOrFail($id);
        return view('backend.category.add-or-edit-cate', ['cate' => $cate]);

    }

    public function postAddOrEditCate(CateRequest $request, $id = 0)
    {
        $data = $request->all();
        if ($id != 0) {
            $cate = Category::findOrFail($id);
        } else {
            $cate = new Category();
        }
        $cate->fill($data);
        $cate->save();
        if ($id != 0) {
            return redirect()->route('ListCate')->with('thongbao', 'sửa Danh Mục Thành Công');
        } else {
            return redirect()->route('ListCate')->with('thongbao', 'Thêm Mới Danh Mục Thành Công');
        }
    }

    public function delCate(Request $request)
    {
        $id = $request->id;
        $cate = Category::findOrFail($id);
        $list = ListCategory::where('category_id', $id)->first();
        if ($list) {
            return response()->json([
                'errors' => false
            ]);
        }
        $cate->delete();
        return response()->json([
            'errors' => true
        ]);
    }
}
