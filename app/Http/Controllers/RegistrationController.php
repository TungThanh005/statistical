<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserFontendRequest;
use App\User;
use Illuminate\Support\Facades\Auth;

class RegistrationController extends Controller
{
    public function AddOrEditUserFontend($id = 0)
    {
        if ($id != 0) {
            $user = User::findOrFail($id);
            return view('fontend.add-or-edit-user', ['user' => $user]);
        } else {
            return view('fontend.add-or-edit-user');
        }
    }

    public function PostAddOrEditUserFontend(UserFontendRequest $request, $id = 0)
    {
        $data = $request->all();
        $password = $request->password;
        if ($id != 0) {
            $user = User::findOrFail($id);
        } else {
            $user = new User();
        }
        $user->fill($data);
        if (!is_null($request->password)) {
            $user->password = bcrypt($request->password);
        }
        $user->remember_token = $request->_token;
        if (!is_null($request->avatar)) {
            $file = $request->avatar;
            $name = $file->getClientOriginalName();
            $path = public_path('image');
            $nameFull = time() . '-' . $name;
            if ($id != 0) {
                unlink('image/' . $user->avatar);
            }
            $file->move($path, $nameFull);
            $user->avatar = $nameFull;
        }
        $user->save();
        Auth::login($user);
        if ($id != 0) {
            return redirect()->route('home')->with('thongbao', 'sửa thông tin thành công');
        } else {
            return redirect()->route('home');
        }
    }
}
