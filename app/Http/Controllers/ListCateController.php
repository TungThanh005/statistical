<?php

namespace App\Http\Controllers;

use App\Http\Requests\ListCateRequest;
use App\Models\Category;
use App\Models\ListCategory;
use App\Models\Product;
use Illuminate\Http\Request;

class ListCateController extends Controller
{
    public function addListCate()
    {
        $cates = Category::all();
        return view('backend.listCategory.add-or-edit-list-cate', ['cates' => $cates]);
    }

    public function listCate()
    {
        $list = ListCategory::paginate(10);
        return view('backend.listCategory.List_category', ['list' => $list]);
    }

    public function editListCate($id)
    {
        $cates=Category::all();
        $listCate = ListCategory::findOrFail($id);
        return view('backend.listCategory.add-or-edit-list-cate', ['listcate' => $listCate, 'cates' => $cates]);
    }

    public function postAddOrEditListCate(ListCateRequest $request, $id = 0)
    {

        $data = $request->all();
        if ($id != 0) {
            $listCate = ListCategory::findOrFail($id);
        } else {
            $listCate = new ListCategory();
        }
        $listCate->fill($data);
        $listCate->save();
        if ($id != 0) {
            return redirect()->route('List_Cate')->with('thongBao', 'sửa thành công');
        } else {
            return redirect()->route('List_Cate')->with('thongBao', 'Thêm Mới thành công');
        }
    }

    public function delListCate(Request $request)
    {
        $id = $request->id;
        $listCate = ListCategory::findOrFail($id);
        $pro = Product::where('listCategory_id', $id)->first();
        if ($pro) {
            return response()->json([
                'errors' => false
            ]);
        } else {
            $listCate->delete();
            return response()->json([
                'errors' => true
            ]);
        }

    }
}
