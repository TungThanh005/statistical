<?php

namespace App\Http\Controllers;

use App\Models\comment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function Comment(Request $request)
    {
        if (Auth::check()) {
            $userId = Auth::user()->id;
            $idpro = $request->product_id;
            $data = $request->all();
            $comment = new comment();
            $comment->fill($data);
            $comment->user_id = $userId;
            $comment->save();
            $listComment = comment::where('product_id', $idpro)->orderBy('created_at', 'DESC')->take(10)->get();
            $dataUser = User::findOrFail($userId);
            $name = $dataUser->name;
            $level = $dataUser->lever;
            return response()->json([
                'data' => $listComment,
                'name' => $name,
                'level' => $level,
                'userId' => $userId
            ]);
        } else {
            return response()->json(
                false);
        }
    }

    public function deleteComment($id)
    {
        $comment = comment::findOrfail($id);
        $comment->delete();
        return back()->with('thongBao', 'bạn vừa xóa 1 comment');
    }
}
