<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserBackendRequest;
use App\Models\comment;
use App\Models\Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function addUser()
    {
        return view('backend.user.add-or-edit-user-backend');
    }

    public function listUser()
    {
        $user = User::paginate(10);
        return view('backend.user.list-user-backend', ['users' => $user]);
    }

    public function editUser($id)
    {
        if ($id) {
            $user = User::findOrFail($id);
            return view('backend.user.add-or-edit-user-backend', ['user' => $user]);
        }
    }

    public function postEditUser(UserBackendRequest $request, $id = 0)
    {

        $data = $request->all();
        $password = $request->password;
        if ($id != 0) {
            $user = User::findOrFail($id);
        } else {
            $user = new User();
        }
        $user->fill($data);
        $user->remember_token = $request->_token;
        if ($id != 0) {
            if (!is_null($request->password)) {
                $user->password = bcrypt($password);
            }
        } else {
            $user->password = bcrypt($password);
        }
        if (!is_null($request->avatar)) {
            $file = $request->avatar;
            if ($id != 0) {
                unlink("image/" . $user->avatar);
            }
            $name = $file->getClientOriginalName();
            $path = public_path('image');
            $names = time() . '-' . $name;
            $file->move($path, $names);
            $user->avatar = $names;
        }
        $user->save();
        if ($id != 0) {
            Auth::login();
            return redirect()->route('ListUser')->with('thongbao', 'Sửa thông tin thành công');
        } else
            return redirect()->route('ListUser')->with('thongbao', 'Thêm mới người dùng thành công');
    }

    public function delUser(Request $request)
    {
        $id = $request->id;
        $user = User::findOrFail($id);
        $com = comment::where('user_id', $id)->first();
        $order = Order::where('user_id', $id)->first();
        if (Auth::user()->lever == $user->lever) {
            return response()->json
            ([
                'errors' => false
            ]);
        } else if ($com || $order) {
            return response()->json
            ([
                'errors' => true
            ]);
        } else {
            unlink('image/' . $user->avatar);
            $user->delete();
            return response()->json
            ([
                'errors' => "khong"
            ]);
        }
    }
}
