<?php

namespace App\Http\Controllers;

use App\Charts\statisticalAgeUser;
use App\Charts\statisticalGenderUsser;
use App\Charts\statisticalPrice;
use App\Charts\statisticalSumQuanlityOrder;
use App\Models\Category;
use App\Models\ListCategory;
use App\Models\OrderDetail;
use App\Models\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatisticalController extends Controller
{
    public function statisticalPrice()
    {

        // thống kê sản phẩm theo giá bán
        $price = Product::select('price', 'name')->get();
        $lables = $price->pluck('name');
        $values = $price->pluck('price');
        $chart = new statisticalPrice();
        $chart->labels($lables);
        $chart->dataset('Biểu đồ thống kê giá sản phẩm', 'bar', $values);

        // thống kê sản phẩm theo số lượng bán
        $sumQuanlityOrder = OrderDetail::with('orderdetail_product')
        ->select(DB::raw('SUM(quanlity) as total_quanlity'), 'product_id')
        ->groupBy('product_id')
        ->havingRaw('SUM(quanlity)')
        ->get();
        $valuesSum = $sumQuanlityOrder->pluck('total_quanlity');
        $lablesSum = [];
        foreach ($sumQuanlityOrder as $values) {
            $lablesSum[] = $values->orderdetail_product->name;
        }
        $chartSum = new statisticalSumQuanlityOrder();
        $chartSum->labels($lablesSum);
        $chartSum->dataset('Biểu đồ thống kê số lượng bán', 'bar', $valuesSum);

        //Thống kê theo độ tuổi
        $ages = User::all();
        $total = $ages->count();
        $age = [];
        $gender = [];
        foreach ($ages as $value) {
            $yearOfBirth = explode('-', $value->birthday);
            $date = getdate();
            $age[] = $date['year'] - $yearOfBirth[0];
            if ($value->gender == 0) {
                $gender[] = 0;
            }
        }
        $percent = [];
        $countAge = [];
        foreach (array_count_values($age) as $key => $value) {
            $countAge[] = $key . 'tuổi';
            $percent[] = ($value * 100) / ($total);
        }
        $chartAge = new statisticalAgeUser();
        $chartAge->labels($countAge);
        $chartAge->dataset('Biểu đồ thống kê tuổi của thành viên', 'doughnut', $percent);

        // thống kê theo giới tính
        $totalGender = count($gender);
        $percentGenderMale = ($totalGender * 100) / $total;
        $valuesGender = array($percentGenderMale, (100 - $percentGenderMale));
        $lablesGender = array('tỉ lệ phần tăm nam là :' . $valuesGender[0] . '%', 'tỉ lệ phần trăm nữ là :' . $valuesGender[1] . '%');
        $chartGender = new statisticalGenderUsser();
        $chartGender->labels($lablesGender);
        $chartGender->dataset('Biểu đồ thống kê tuổi của thành viên', 'doughnut', $valuesGender);
        return view('backend.statistical.statistical_price', ['chart' => $chart, 'chartSum' => $chartSum, 'chartAge' => $chartAge, 'chartGender' => $chartGender]);
    }

    public function statisticalThePeriod()
    {
        return view('backend.statistical.statistical-the-period');
    }

    public function statisticalSearchDateThePeriod(Request $request)
    {
        /*return ($request->all());*/
        $countOrder = OrderDetail::with('orderdetail_product')
            ->select('product_id', DB::raw('SUM(quanlity)as total_quanlity'))
            ->whereBetween('created_at', [$request->startDate, $request->endDate])
            ->groupBy('product_id')
            ->havingRaw('SUM(quanlity)')
            ->get();
        return response()->json($countOrder);
    }

    public function StatisticalProduct()
    {
        return view('backend.statistical.st-product.select-st-product');
    }

    public function StatisticalProductForm()
    {
        $cateLists = ListCategory::all();
        $cates = Category::all();
        return view('backend.statistical.form-search-product-st', ['cateLists' => $cateLists, 'cates' => $cates]);
    }

    public function PostStatisticalProductForm(Request $request)
    {
        $cate = $request->select_cate;
        $listCate = $request->select_listCate;
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $price_min = $request->price_min;
        $price_max = $request->price_max;
        $data = DB::table('order_detail as od')->select('pro.name', DB::raw('SUM(od.quanlity)as total_quanlity'), 'od.product_id')
            ->join('product as pro', 'pro.id', '=', 'od.product_id')
            ->join('list_category as lc', 'lc.id', '=', 'pro.listCategory_id')
            ->join('category as cate', 'cate.id', '=', 'lc.category_id')
            ->where(function ($query) use ($cate, $listCate) {
                if ($listCate == null && $cate != null) {
                    $query->where('cate.id', '=', $cate);
                } else if ($listCate != null) {
                    $query->where('lc.id', '=', $listCate);
                }
            })
            ->where(function ($qurey) use ($startDate, $endDate) {
                if ($startDate != null && $endDate != null) {
                    $qurey->whereBetween('od.created_at', [$startDate, $endDate]);
                }
            })
            ->where(function ($qurey) use ($price_max, $price_min) {
                if ($price_max != null && $price_min != null) {
                    $qurey->whereBetween('pro.price', [$price_min, $price_max]);
                }
            })
            ->groupBy('od.product_id', 'pro.name')
            ->havingRaw('SUM(od.quanlity)')
            ->get();
        $key = [];
        $values = [];
        foreach ($data as $value) {
            $values[] = $value->total_quanlity;
            $key[] = $value->name;
        }
        return response()->json([
            $key, $values
        ]);
        /* $chartPro = new statisticalGenderUsser();
         $chartPro->labels($key);
         $chartPro->dataset('Biểu đồ thống kê sản phẩm theo số lượng sản phẩm bán ra', 'bar', $values);
         return view('backend.statistical.statistical_price', ['chartGender' => $chartPro]);*/
    }

    public function StatisticalProductViewForm()
    {
        $cateLists = ListCategory::all();
        $cates = Category::all();
        return view('backend.statistical.form-search-product-view-st', ['cateLists' => $cateLists, 'cates' => $cates]);
    }

    public function StatisticalProductPriceForm()
    {
        $cateLists = ListCategory::all();
        $cates = Category::all();
        return view('backend.statistical.form-search-product-price-st', ['cateLists' => $cateLists, 'cates' => $cates]);
    }

    public function PostStatisticalProductViewForm(Request $request)
    {
        $cate = $request->select_cate;
        $listCate = $request->select_listCate;
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $price_min = $request->price_min;
        $price_max = $request->price_max;
        $data = DB::table('product as pro')->select('pro.name', 'pro.view', 'pro.price')
            ->join('list_category as lc', 'lc.id', '=', 'pro.listCategory_id')
            ->join('category as cate', 'cate.id', '=', 'lc.category_id')
            ->where(function ($query) use ($cate, $listCate) {
                if ($listCate == null && $cate != null) {
                    $query->where('cate.id', '=', $cate);
                } else if ($listCate != null) {
                    $query->where('lc.id', '=', $listCate);
                }
            })
            ->where(function ($qurey) use ($startDate, $endDate) {
                if ($startDate != null && $endDate != null) {
                    $qurey->whereBetween('pro.created_at', [$startDate, $endDate]);
                }
            })
            ->where(function ($qurey) use ($price_max, $price_min) {

                if ($price_max != null && $price_min != null) {
                    $qurey->whereBetween('pro.price', [$price_min, $price_max]);
                } else if ($price_max != null && $price_min == null) {
                    $qurey->where('pro.price', $price_max);
                } else if ($price_max == null && $price_min != null) {
                    $qurey->where('pro.price', $price_min);
                }
            })
            ->get();
        $key = [];
        $values = [];
        $valuess = [];
        foreach ($data as $value) {
            $values[] = $value->view;
            $valuess[] = $value->price;
            $key[] = $value->name;
        }
        return response()->json([
            $key, $values, $valuess
        ]);
    }
}
