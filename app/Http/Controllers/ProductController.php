<?php

namespace App\Http\Controllers;

use App\Exports\ProductExport;
use App\Http\Requests\ProRequest;
use App\Models\Category;
use App\Models\comment;
use App\Models\ListCategory;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\Sale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;


class ProductController extends Controller
{
    public function addProduct()
    {
        $cate = Category::all();
        return view('backend.product.addProduct', ['cates' => $cate]);
    }

    public function ajaxProduct(Request $request)
    {
        $id = $request->id;
        $listCate = ListCategory::where('category_id', $id)->where('status', 1)->get();
        $list = ['<option value="" >Xin mời chọn</option>'];
        foreach ($listCate as $value) {
            array_push($list, '<option value="' . $value->id . '" >' . $value->name . '</option>');
        }
        return $list;
    }

    public function ajaxSelectedEditListacte(Request $request)
    {
        $id = $request->id;
        $idpro = $request->idpro;
        $listCate = ListCategory::where('category_id', $id)->where('status', 1)->get();
        $product = Product::findOrfail($idpro);
        $list = [];
        foreach ($listCate as $value) {
            if ($value->id == $product->listCategory_id) {
                $list[] = '<option selected value="' . $value->id . '" >' . $value->name . '</option>';
            }
            if ($value->id != $product->listCategory_id) {
                $list[] = '<option value="' . $value->id . '" >' . $value->name . '</option>';
            }
        }
        return $list;
    }

    public function postAddProduct(AddProductRequest $request)
    {
        $data = $request->all();
        $pro = new Product();
        $pro->fill($data);
        $pro->view = 0;
        $file = $request->image;
        $name = $file->getClientOriginalName();
        $path = public_path('image');
        $nameFull = time() . '-' . $name;
        $file->move($path, $nameFull);
        $pro->image = $nameFull;
        $pro->save();
        return redirect()->route('listProduct')->with('thongbao', 'thêm mới sản phẩm thành công');
    }

    public function listProduct()
    {
        $product = Product::paginate(10);
        return view('backend.product.list-product', ['pro' => $product]);
    }

    public function AddOrEditProduct($id = 0)
    {
        if ($id != 0) {
            $sale = Sale::where('product_id', $id)->first();
            $ca = Category::all();
            $pro = Product::findOrFail($id);
            $list = $pro->listCategory_id;
            $cateList = DB::table('product as pro')->select(['cate.id', 'cate.name'])
                ->join('list_category as l', 'l.id', '=', 'pro.listCategory_id')
                ->join('category as cate', 'cate.id', '=', 'l.category_id')
                ->where('pro.listCategory_id', $list)->first();
            $cate = ListCategory::where('status', 1)->get();
            $listcate1 = ListCategory::all();
            return view('backend.product.add-or-edit-product', ['pro' => $pro, 'catelist' => $cateList, 'cate' => $cate, 'cates' => $ca, 'sale' => $sale, 'listcate' => $listcate1]);
        } else {
            $cate = Category::all();
            $listcate1 = ListCategory::all();
            return view('backend.product.add-or-edit-product', ['cates' => $cate, 'listcate' => $listcate1]);
        }
    }

    public function postAddOrEditProduct(ProRequest $request, $id = 0)
    {

        $dis_count = $request->dis_count;
        $data = $request->all();
        if ($id != 0) {
            $pro = Product::FindOrFail($id);
        } else {
            $pro = new Product();
        }

        $pro->fill($data);
        if (!is_null($request->images)) {
            $file = $request->images;
            $name = $file->getClientOriginalName();
            $path = public_path('image/');
            $nameFull = time() . '-' . $name;
            if ($id != 0) {
                unlink('image/' . $pro->image);
            }
            $file->move($path, $nameFull);
            $pro->image = $nameFull;
        }
        $pro->save();
        if ($id != 0) {
            $sale = Sale::where('product_id', $id)->first();
            if (!is_null($dis_count)) {
                if ($sale) {
                    $sale->fill($data);
                    $sale->product_id = $id;
                    $sale->save();
                } else {
                    $newSale = new Sale();
                    $newSale->fill($data);
                    $newSale->product_id = $pro->id;
                    $newSale->save();
                }
            } else {
                if ($sale) {
                    $sale->delete();
                }
            }
            return redirect()->route('listProduct')->with('thongbao', 'sửa sản phẩm thành công');
        } else {
            if (!is_null($dis_count)) {
                $sale = new Sale();
                $sale->fill($data);
                $sale->product_id = $pro->id;
                $sale->save();
            }
            return redirect()->route('listProduct')->with('thongbao', 'Thêm mới sản phẩm thành công');
        }
    }

    public function deleteProduct(Request $request)
    {
        $id = $request->id;
        $pro = Product::FindOrFail($id);
        $sale = Sale::where('product_id', $id);
        $com = comment::where('product_id', $id)->first();
        $order = OrderDetail::where('product_id', $id)->first();
        if ($com || $order) {
            return response()->json([
                'errors' => false
            ]);
        } else {
            if ($sale) {
                $sale->delete();
            }
            unlink('image/' . $pro->image);
            $pro->delete();
            return response()->json([
                'errors' => true
            ]);
        }
    }

    public function search(Request $request)
    {
        $key = $request->key;
        $converes = explode(' ', $key);
        $data = Product::query();
        foreach ($converes as $word) {
            $data->orWhere('price', 'like', '%' . $word . '%')
                ->orWhere('image', 'like', '%' . $word . '%')
                ->orWhere('description', 'like', '%' . $word . '%')
                ->orWhere('name', 'like', '%' . $word . '%');
        }
        $data = $data->distinct()->get();
        return view('fontend.search-product', ['search' => $data, 'key' => $key]);
    }

    public function export()
    {

        return Excel::download(new ProductExport(), 'tung1.xlsx');
    }

}
