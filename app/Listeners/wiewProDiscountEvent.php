<?php

namespace App\Listeners;

use App\Events\DiscountEvent;

class wiewProDiscountEvent
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DiscountEvent $event
     * @return void
     */
    public function handle(DiscountEvent $event)
    {
        $event->product->increment('view');
    }
}
