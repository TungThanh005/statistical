<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Product;
use App\Models\Slide;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $slide = Slide::where('status', 1)->get();
        $category = Category::where('status', 1)->get();
        $pro = Product::where('status', 1)->take(8)->orderby('created_at', 'desc')->get();
        $pr = \Illuminate\Support\Facades\DB::select("SELECT product_id FROM `order_detail` 
        GROUP BY product_id
        order BY SUM(quanlity) DESC 
        limit 0,8");
        $products = [];
        $viewproduct = Product::all()->sortByDesc('view')->take(8);
        foreach ($pr as $key => $value) {
            $products[$key] = Product::find($value->product_id);

        }


        view()->share(['allslide' => $slide, 'category' => $category, 'product' => $pro, 'products' => $products, 'viewpro' => $viewproduct]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
