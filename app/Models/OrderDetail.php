<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'order_detail';
    protected $fillable = ['quanlity', 'price', 'order_id', 'product_id'];

    public function orderdetail_order()
    {
        return $this->hasOne(order::class, 'order_id', 'id');
    }

    public function orderdetail_product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
