<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    protected $table = 'comment';
    protected $fillable = ['product_id', 'content'];

    public function comment_user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
