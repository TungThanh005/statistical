<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';
    protected $fillable = ['name', 'email', 'status', 'phone', 'address'];

    public function order_orderdetail()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function order_user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }


}
