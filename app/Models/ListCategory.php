<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ListCategory extends Model
{
    protected $table = 'list_category';
    protected $fillable = ['name', 'category_id', 'status'];

    public function listcate_cate()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function listcate_pro()
    {
        return $this->hasMany(Product::class, 'listCategory_id', 'id');
    }
}
