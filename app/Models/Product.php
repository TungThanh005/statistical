<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    protected $table = 'product';
    protected $fillable = ['name', 'listCategory_id', 'status', 'price', 'view', 'description'];

    public function pro_listcate()
    {
        return $this->belongsTo(ListCategory::class, 'listCategory_id', 'id');
    }

    public function category($listCategory_id)
    {
        $cate = DB::table('product as pro')->select('cate.name')
            ->join('list_category as l', 'l.id', '=', 'pro.listCategory_id')
            ->join('category as cate', 'cate.id', '=', 'l.category_id')
            ->where('pro.listCategory_id', $listCategory_id)->first();
        return $cate;
    }

    public function product_sale()
    {
        return $this->hasOne(Sale::class, 'product_id', 'id');
    }

}
