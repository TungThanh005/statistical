-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 26, 2018 lúc 05:44 AM
-- Phiên bản máy phục vụ: 10.1.33-MariaDB
-- Phiên bản PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `thuctap`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(5, 'Đồ Uống', 1, '2018-09-24 10:14:04', '2018-10-17 10:16:02'),
(6, 'Đồ Ăn Đóng Gói', 1, '2018-09-27 09:35:17', '2018-10-15 03:12:28'),
(9, 'Bánh Ngọt', 1, '2018-10-04 09:14:20', '2018-10-15 03:12:50'),
(10, 'bột nêm', 1, '2018-10-10 01:56:40', '2018-10-15 03:13:11'),
(13, 'Dụng Cụ', 1, '2018-10-15 03:13:33', '2018-10-15 03:13:33'),
(14, 'Rau quả', 1, '2018-10-15 03:14:00', '2018-10-15 03:14:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comment`
--

CREATE TABLE `comment` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `comment`
--

INSERT INTO `comment` (`id`, `product_id`, `user_id`, `content`, `created_at`, `updated_at`) VALUES
(1, 10, 15, 'ww', '2018-10-04 07:15:38', '2018-10-04 07:15:38'),
(2, 10, 15, 'wwâs', '2018-10-04 07:16:15', '2018-10-04 07:16:15'),
(3, 10, 15, 'wwâssâs', '2018-10-04 07:16:27', '2018-10-04 07:16:27'),
(4, 10, 15, 'wwâssâs', '2018-10-04 07:16:29', '2018-10-04 07:16:29'),
(5, 10, 15, 'sâsá', '2018-10-04 07:16:35', '2018-10-04 07:16:35'),
(6, 10, 15, 'ee', '2018-10-04 07:19:31', '2018-10-04 07:19:31'),
(7, 10, 15, 'd', '2018-10-04 07:20:25', '2018-10-04 07:20:25'),
(8, 10, 15, 'c', '2018-10-04 07:20:48', '2018-10-04 07:20:48'),
(9, 10, 15, 'd', '2018-10-04 07:21:19', '2018-10-04 07:21:19'),
(10, 10, 15, 's', '2018-10-04 07:23:39', '2018-10-04 07:23:39'),
(11, 10, 15, 'd', '2018-10-04 07:24:46', '2018-10-04 07:24:46'),
(12, 10, 15, 's', '2018-10-04 07:25:15', '2018-10-04 07:25:15'),
(13, 10, 15, 'd', '2018-10-04 07:26:14', '2018-10-04 07:26:14'),
(14, 10, 15, 'tao chi muon chăc chan thoi', '2018-10-04 07:26:36', '2018-10-04 07:26:36'),
(15, 10, 15, 'd', '2018-10-04 07:28:00', '2018-10-04 07:28:00'),
(16, 10, 15, 'đ', '2018-10-04 07:30:42', '2018-10-04 07:30:42'),
(17, 10, 15, 'd', '2018-10-04 07:31:00', '2018-10-04 07:31:00'),
(18, 10, 15, 'sdsdsd', '2018-10-04 07:31:45', '2018-10-04 07:31:45'),
(19, 10, 15, 'sđsdsd', '2018-10-04 07:38:27', '2018-10-04 07:38:27'),
(20, 10, 15, 'sđsdsddsd', '2018-10-04 07:38:30', '2018-10-04 07:38:30'),
(21, 10, 15, 'sđsdsddsdsdsd', '2018-10-04 07:38:32', '2018-10-04 07:38:32'),
(22, 10, 15, 'sâs', '2018-10-04 07:43:56', '2018-10-04 07:43:56'),
(27, 10, 15, 'rat la moi', '2018-10-04 07:50:16', '2018-10-04 07:50:16'),
(28, 10, 15, 'Rất là mới', '2018-10-04 07:50:33', '2018-10-04 07:50:33'),
(29, 10, 15, 'rất la mới', '2018-10-04 07:52:13', '2018-10-04 07:52:13'),
(30, 10, 15, 'qqqqqqqqqqqqq', '2018-10-04 07:52:44', '2018-10-04 07:52:44'),
(31, 10, 15, 'rat la moi', '2018-10-04 07:53:46', '2018-10-04 07:53:46'),
(32, 10, 15, 'aaaaaâ', '2018-10-04 07:55:20', '2018-10-04 07:55:20'),
(33, 10, 15, 'aaaaaaaaaaaaaaaaaaâ', '2018-10-04 07:55:41', '2018-10-04 07:55:41'),
(34, 10, 15, 'tao al ai', '2018-10-04 07:56:27', '2018-10-04 07:56:27'),
(35, 10, 15, 'toi la toi', '2018-10-04 08:01:16', '2018-10-04 08:01:16'),
(36, 10, 15, 'rrrrrrrrrr', '2018-10-04 08:02:01', '2018-10-04 08:02:01'),
(37, 10, 15, 'rrrrrrrrrr', '2018-10-04 08:02:02', '2018-10-04 08:02:02'),
(38, 10, 15, 'rrrrrrrrrrrrrrs', '2018-10-04 08:02:08', '2018-10-04 08:02:08'),
(39, 10, 15, 'cvcvcv', '2018-10-04 08:03:36', '2018-10-04 08:03:36'),
(40, 10, 15, 'cvcvcvaâ', '2018-10-04 08:03:45', '2018-10-04 08:03:45'),
(41, 10, 15, 'cvcvcvaâq', '2018-10-04 08:04:01', '2018-10-04 08:04:01'),
(42, 10, 15, 'cvcvcvaâqaqqqqqqqqqqqqqqqq', '2018-10-04 08:04:21', '2018-10-04 08:04:21'),
(43, 10, 15, 'wwwwwwwwwwwwwwwwwwww', '2018-10-04 08:07:02', '2018-10-04 08:07:02'),
(44, 10, 15, 'wwwwwwwwwwwwwwwwwwww', '2018-10-04 08:07:06', '2018-10-04 08:07:06'),
(45, 10, 15, 'wwwwwwwwwwwwwwwwwwww', '2018-10-04 08:07:10', '2018-10-04 08:07:10'),
(46, 10, 15, 'hahâ', '2018-10-04 08:10:15', '2018-10-04 08:10:15'),
(47, 10, 15, 'ss', '2018-10-04 08:13:53', '2018-10-04 08:13:53'),
(48, 10, 15, 'tttttttt', '2018-10-04 08:14:32', '2018-10-04 08:14:32'),
(49, 10, 15, 'tttttttt', '2018-10-04 08:14:36', '2018-10-04 08:14:36'),
(50, 10, 15, 'tttttttt', '2018-10-04 08:14:39', '2018-10-04 08:14:39'),
(51, 10, 15, 'tttttttt', '2018-10-04 08:14:41', '2018-10-04 08:14:41'),
(52, 10, 15, 'tttttttt', '2018-10-04 08:14:41', '2018-10-04 08:14:41'),
(53, 10, 15, 'dđ', '2018-10-04 08:14:52', '2018-10-04 08:14:52'),
(54, 10, 15, 'dđ', '2018-10-04 08:14:54', '2018-10-04 08:14:54'),
(55, 10, 15, 'dđ', '2018-10-04 08:14:55', '2018-10-04 08:14:55'),
(56, 10, 15, 'dđ', '2018-10-04 08:14:56', '2018-10-04 08:14:56'),
(57, 10, 15, 'đ', '2018-10-04 08:15:10', '2018-10-04 08:15:10'),
(58, 10, 15, 'đ', '2018-10-04 08:15:12', '2018-10-04 08:15:12'),
(59, 10, 15, 'đ', '2018-10-04 08:15:12', '2018-10-04 08:15:12'),
(60, 10, 15, 'đ', '2018-10-04 08:15:13', '2018-10-04 08:15:13'),
(61, 10, 15, 'đ', '2018-10-04 08:15:13', '2018-10-04 08:15:13'),
(62, 10, 15, 'đ', '2018-10-04 08:15:13', '2018-10-04 08:15:13'),
(63, 10, 15, 'đ', '2018-10-04 08:15:13', '2018-10-04 08:15:13'),
(64, 10, 15, 'đ', '2018-10-04 08:15:14', '2018-10-04 08:15:14'),
(65, 10, 15, 'đ', '2018-10-04 08:15:14', '2018-10-04 08:15:14'),
(66, 10, 15, 'đ', '2018-10-04 08:15:14', '2018-10-04 08:15:14'),
(67, 10, 15, 'đ', '2018-10-04 08:15:15', '2018-10-04 08:15:15'),
(68, 10, 15, 'đ', '2018-10-04 08:15:15', '2018-10-04 08:15:15'),
(69, 10, 15, 'đ', '2018-10-04 08:15:30', '2018-10-04 08:15:30'),
(70, 10, 15, 'fdfd', '2018-10-04 08:15:48', '2018-10-04 08:15:48'),
(71, 10, 15, 'd', '2018-10-04 08:16:18', '2018-10-04 08:16:18'),
(72, 10, 15, 'd', '2018-10-04 08:16:19', '2018-10-04 08:16:19'),
(73, 10, 15, 'd', '2018-10-04 08:16:20', '2018-10-04 08:16:20'),
(74, 10, 15, 's', '2018-10-04 08:16:27', '2018-10-04 08:16:27'),
(75, 10, 15, 'ạkl;', '2018-10-04 08:19:44', '2018-10-04 08:19:44'),
(76, 10, 15, 'ewewe', '2018-10-04 08:21:53', '2018-10-04 08:21:53'),
(77, 10, 15, 'eweweưewe', '2018-10-04 08:21:57', '2018-10-04 08:21:57'),
(78, 10, 15, 'eweweưeweưewewe', '2018-10-04 08:21:59', '2018-10-04 08:21:59'),
(79, 10, 15, 'ssssssss', '2018-10-04 08:23:50', '2018-10-04 08:23:50'),
(80, 10, 15, 'ssssssssdđ', '2018-10-04 08:23:54', '2018-10-04 08:23:54'),
(81, 10, 15, 'tytytyt', '2018-10-04 08:24:11', '2018-10-04 08:24:11'),
(82, 10, 15, 'sdsdsd', '2018-10-04 08:25:12', '2018-10-04 08:25:12'),
(83, 10, 15, 'sd', '2018-10-04 08:26:03', '2018-10-04 08:26:03'),
(84, 10, 15, 'sâs', '2018-10-04 08:26:38', '2018-10-04 08:26:38'),
(85, 10, 15, 'ssssssssss', '2018-10-04 08:29:26', '2018-10-04 08:29:26'),
(86, 10, 15, 's', '2018-10-04 08:30:45', '2018-10-04 08:30:45'),
(87, 10, 15, 's', '2018-10-04 08:31:08', '2018-10-04 08:31:08'),
(88, 10, 15, 's', '2018-10-04 08:31:33', '2018-10-04 08:31:33'),
(89, 10, 15, 'ss', '2018-10-04 08:32:34', '2018-10-04 08:32:34'),
(90, 10, 15, 'd', '2018-10-04 08:33:28', '2018-10-04 08:33:28'),
(91, 10, 15, 'd', '2018-10-04 08:33:52', '2018-10-04 08:33:52'),
(92, 10, 15, 's', '2018-10-04 08:34:48', '2018-10-04 08:34:48'),
(93, 10, 15, 'd', '2018-10-04 08:35:28', '2018-10-04 08:35:28'),
(94, 10, 15, 'd', '2018-10-04 08:37:09', '2018-10-04 08:37:09'),
(95, 10, 15, 's', '2018-10-04 08:37:32', '2018-10-04 08:37:32'),
(96, 10, 15, 's', '2018-10-04 08:37:35', '2018-10-04 08:37:35'),
(97, 10, 15, 's', '2018-10-04 08:37:37', '2018-10-04 08:37:37'),
(98, 10, 15, 'ss', '2018-10-04 08:37:42', '2018-10-04 08:37:42'),
(99, 10, 15, 'dd', '2018-10-04 08:37:49', '2018-10-04 08:37:49'),
(100, 10, 15, 'ddđ', '2018-10-04 08:37:51', '2018-10-04 08:37:51'),
(101, 10, 15, 'ddđ', '2018-10-04 08:37:52', '2018-10-04 08:37:52'),
(102, 10, 15, 'tung', '2018-10-04 08:37:58', '2018-10-04 08:37:58'),
(103, 10, 15, 'tungthanh', '2018-10-04 08:39:56', '2018-10-04 08:39:56'),
(104, 10, 15, 'tungthanh nguyenw', '2018-10-04 08:40:01', '2018-10-04 08:40:01'),
(105, 10, 15, 'tungthanh nguyenw da', '2018-10-04 08:40:22', '2018-10-04 08:40:22'),
(106, 10, 15, 'sdsdsd', '2018-10-04 08:41:43', '2018-10-04 08:41:43'),
(107, 10, 15, 'sâs', '2018-10-04 08:41:46', '2018-10-04 08:41:46'),
(108, 10, 15, 'haha', '2018-10-04 08:42:53', '2018-10-04 08:42:53'),
(109, 10, 15, 'sds', '2018-10-04 08:42:56', '2018-10-04 08:42:56'),
(110, 10, 15, 'mai', '2018-10-04 08:44:27', '2018-10-04 08:44:27'),
(111, 10, 15, 'ko', '2018-10-04 08:44:30', '2018-10-04 08:44:30'),
(112, 10, 15, 'xog', '2018-10-04 08:44:41', '2018-10-04 08:44:41'),
(113, 10, 15, 'de', '2018-10-04 08:44:58', '2018-10-04 08:44:58'),
(114, 10, 15, 'de', '2018-10-04 08:45:00', '2018-10-04 08:45:00'),
(115, 10, 15, 'ed', '2018-10-04 08:45:03', '2018-10-04 08:45:03'),
(116, 10, 15, 'qư', '2018-10-04 08:45:26', '2018-10-04 08:45:26'),
(117, 10, 15, 'ưq', '2018-10-04 08:45:28', '2018-10-04 08:45:28'),
(118, 10, 15, 'hyhy', '2018-10-04 08:46:05', '2018-10-04 08:46:05'),
(121, 10, 15, 'rf', '2018-10-04 08:46:40', '2018-10-04 08:46:40'),
(122, 10, 15, 'fr', '2018-10-04 08:46:45', '2018-10-04 08:46:45'),
(137, 12, 15, 'comment gi bay gio', '2018-10-04 10:52:06', '2018-10-04 10:52:06'),
(138, 5, 15, 'dáđá', '2018-10-04 10:55:30', '2018-10-04 10:55:30'),
(139, 5, 15, 'sâsá', '2018-10-04 10:55:33', '2018-10-04 10:55:33'),
(140, 5, 15, 'dsd', '2018-10-04 10:56:34', '2018-10-04 10:56:34'),
(141, 5, 15, 'dsdsd', '2018-10-04 10:56:36', '2018-10-04 10:56:36'),
(142, 5, 15, 'sdsdsd', '2018-10-04 10:56:38', '2018-10-04 10:56:38'),
(145, 13, 15, 'Tungthanh', '2018-10-11 03:57:21', '2018-10-11 03:57:21'),
(146, 13, 15, 'sas', '2018-10-11 04:11:39', '2018-10-11 04:11:39'),
(147, 13, 15, 'ssas', '2018-10-11 04:12:02', '2018-10-11 04:12:02'),
(148, 13, 15, 'sasas', '2018-10-11 04:12:33', '2018-10-11 04:12:33'),
(149, 13, 15, 'sas', '2018-10-11 04:12:36', '2018-10-11 04:12:36'),
(151, 13, 15, 'ewe', '2018-10-11 04:17:36', '2018-10-11 04:17:36'),
(152, 13, 15, 'dsdsd', '2018-10-11 04:18:16', '2018-10-11 04:18:16'),
(153, 12, 15, 'ewewe', '2018-10-11 04:21:36', '2018-10-11 04:21:36'),
(155, 12, 15, 'as', '2018-10-11 04:25:56', '2018-10-11 04:25:56'),
(156, 12, 15, 'tungthanh', '2018-10-11 04:26:05', '2018-10-11 04:26:05'),
(157, 12, 15, 'q', '2018-10-11 04:26:11', '2018-10-11 04:26:11'),
(158, 12, 15, 'huhu', '2018-10-11 04:26:36', '2018-10-11 04:26:36'),
(159, 12, 15, 'dsd', '2018-10-11 04:26:51', '2018-10-11 04:26:51'),
(160, 12, 15, 'tao al ai', '2018-10-11 04:26:56', '2018-10-11 04:26:56'),
(161, 12, 15, 'sanr pham nay khong tot', '2018-10-11 04:30:11', '2018-10-11 04:30:11'),
(162, 12, 15, 'tao thay vay', '2018-10-11 04:30:19', '2018-10-11 04:30:19'),
(163, 12, 15, 'sdsd\nsdsd', '2018-10-11 04:30:24', '2018-10-11 04:30:24'),
(166, 15, 15, 'hahaa', '2018-10-11 07:53:13', '2018-10-11 07:53:13'),
(167, 15, 15, 'hyhy', '2018-10-11 07:53:20', '2018-10-11 07:53:20'),
(168, 13, 15, 'hay qua', '2018-10-11 08:34:29', '2018-10-11 08:34:29'),
(169, 13, 15, 'sdsdsd', '2018-10-11 08:36:00', '2018-10-11 08:36:00'),
(170, 13, 15, 'sâs', '2018-10-11 08:36:33', '2018-10-11 08:36:33'),
(171, 13, 15, 'ssá', '2018-10-11 08:36:52', '2018-10-11 08:36:52'),
(172, 13, 15, 'sâsa', '2018-10-11 08:36:59', '2018-10-11 08:36:59'),
(173, 13, 15, 'sâs', '2018-10-11 08:41:55', '2018-10-11 08:41:55'),
(174, 13, 15, 'dsdsd', '2018-10-11 08:43:15', '2018-10-11 08:43:15'),
(178, 24, 15, 'haha', '2018-10-13 14:44:17', '2018-10-13 14:44:17'),
(179, 12, 15, 'deo mua', '2018-10-13 17:26:09', '2018-10-13 17:26:09'),
(180, 28, 15, 'ấ', '2018-10-14 07:54:56', '2018-10-14 07:54:56'),
(181, 27, 15, 'ád', '2018-10-14 07:55:09', '2018-10-14 07:55:09'),
(191, 13, 17, 'sâsa', '2018-10-16 02:41:50', '2018-10-16 02:41:50'),
(193, 13, 17, 's', '2018-10-16 02:45:14', '2018-10-16 02:45:14'),
(194, 13, 17, 's', '2018-10-16 02:45:56', '2018-10-16 02:45:56'),
(195, 46, 15, 'haha', '2018-10-19 06:48:03', '2018-10-19 06:48:03');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `list_category`
--

CREATE TABLE `list_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `list_category`
--

INSERT INTO `list_category` (`id`, `name`, `status`, `category_id`, `created_at`, `updated_at`) VALUES
(2, 'nước cam', 1, 5, '2018-09-26 03:40:15', '2018-10-15 03:15:58'),
(3, 'nước có ga', 1, 5, '2018-09-27 01:37:05', '2018-09-27 03:43:48'),
(5, 'Sữa', 1, 5, '2018-09-27 09:35:36', '2018-10-15 03:16:18'),
(9, 'Bánh Mỳ', 1, 9, '2018-10-11 10:28:04', '2018-10-15 03:16:48'),
(11, 'Bột Ngọt', 1, 10, '2018-10-15 03:17:10', '2018-10-15 03:17:10'),
(12, 'đồ ăn cho mèo', 1, 6, '2018-10-15 03:17:56', '2018-10-15 03:17:56'),
(13, 'đồ ăn cho người', 1, 6, '2018-10-15 03:18:14', '2018-10-15 03:18:14'),
(14, 'thức ăn cho chó', 1, 6, '2018-10-15 03:18:49', '2018-10-15 04:01:25'),
(15, 'chất tẩy rửa', 1, 13, '2018-10-15 03:19:23', '2018-10-15 03:19:23'),
(16, 'Nồi', 1, 13, '2018-10-15 03:19:51', '2018-10-15 03:19:51'),
(17, 'Rau', 1, 14, '2018-10-15 03:20:17', '2018-10-15 03:20:17'),
(18, 'Quả', 1, 14, '2018-10-15 03:20:31', '2018-10-15 03:20:31');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2013_09_21_080859_category', 1),
(2, '2013_09_21_080936_list_category', 1),
(3, '2013_09_21_081011_product', 1),
(5, '2013_09_21_090619_silde', 1),
(6, '2014_10_12_000000_create_users_table', 1),
(7, '2014_10_12_100000_create_password_resets_table', 1),
(8, '2018_09_21_081134_order', 1),
(9, '2018_09_21_081200_order_detail', 1),
(10, '2018_09_21_081233_comment', 1),
(11, '2013_09_21_081027_sale', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order`
--

CREATE TABLE `order` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `order`
--

INSERT INTO `order` (`id`, `name`, `email`, `phone`, `address`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(13, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 08:03:19', '2018-10-09 08:03:19'),
(18, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 08:13:29', '2018-10-19 07:38:49'),
(24, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 08:42:53', '2018-10-19 07:38:51'),
(26, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 08:47:42', '2018-10-19 07:38:47'),
(27, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 08:48:14', '2018-10-19 07:38:38'),
(28, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 08:52:37', '2018-10-19 07:38:41'),
(29, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 08:52:52', '2018-10-19 07:38:35'),
(31, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 08:55:32', '2018-10-19 07:38:52'),
(32, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 08:57:00', '2018-10-19 07:38:55'),
(33, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 08:59:28', '2018-10-19 07:41:47'),
(34, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:03:07', '2018-10-19 07:38:40'),
(35, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:04:25', '2018-10-19 07:38:37'),
(36, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:04:33', '2018-10-19 07:38:34'),
(37, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:05:19', '2018-10-19 07:38:44'),
(38, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:05:28', '2018-10-19 07:38:30'),
(39, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:05:38', '2018-10-19 07:38:32'),
(40, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:05:57', '2018-10-19 07:38:16'),
(41, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:08:16', '2018-10-19 07:38:15'),
(42, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:09:51', '2018-10-19 07:38:12'),
(43, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:10:04', '2018-10-19 07:38:11'),
(44, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:14:10', '2018-10-19 07:38:09'),
(45, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:20:34', '2018-10-19 07:38:08'),
(46, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:22:01', '2018-10-19 07:38:05'),
(47, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:22:30', '2018-10-19 07:38:03'),
(48, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:24:12', '2018-10-19 07:38:02'),
(49, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:24:44', '2018-10-10 10:09:53'),
(50, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:26:23', '2018-10-10 10:09:46'),
(51, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:28:24', '2018-10-10 10:08:55'),
(52, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:31:12', '2018-10-10 10:03:39'),
(53, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:32:06', '2018-10-10 10:02:33'),
(54, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:36:02', '2018-10-10 10:02:12'),
(55, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:37:21', '2018-10-10 10:01:50'),
(56, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:39:24', '2018-10-10 09:54:11'),
(57, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:39:51', '2018-10-10 09:53:18'),
(58, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-09 09:40:27', '2018-10-10 09:52:34'),
(59, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-14 10:15:55', '2018-10-19 07:38:00'),
(60, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-14 10:18:51', '2018-10-19 07:37:59'),
(61, 'new user', 'tungthadddddhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 17, '2018-10-16 03:02:33', '2018-10-16 08:27:24'),
(62, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-17 04:17:58', '2018-10-19 07:37:57'),
(63, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 1, 15, '2018-10-17 04:18:52', '2018-10-17 04:18:52'),
(64, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 2, 15, '2018-10-17 04:25:11', '2018-10-18 10:02:08'),
(65, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 1, 15, '2018-10-19 08:43:52', '2018-10-19 08:43:52'),
(66, 'tung', 'tungthanhnp@gmail.com', 989251692, 'nam phu-nam phong', 1, 15, '2018-10-24 03:21:44', '2018-10-24 03:21:44');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_detail`
--

CREATE TABLE `order_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `quanlity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `order_detail`
--

INSERT INTO `order_detail` (`id`, `quanlity`, `price`, `order_id`, `product_id`, `created_at`, `updated_at`) VALUES
(19, 1, 12, 13, 10, '2018-10-09 08:03:19', '2018-10-09 08:03:19'),
(20, 1, 12, 18, 10, '2018-10-09 08:13:29', '2018-10-09 08:13:29'),
(21, 1, 120000, 24, 11, '2018-10-09 08:42:53', '2018-10-09 08:42:53'),
(22, 1, 120000, 29, 11, '2018-10-09 08:52:52', '2018-10-09 08:52:52'),
(24, 1, 120000, 31, 11, '2018-10-09 08:55:32', '2018-10-09 08:55:32'),
(25, 1, 120000, 32, 11, '2018-10-09 08:57:00', '2018-10-09 08:57:00'),
(26, 1, 120000, 33, 11, '2018-10-09 08:59:28', '2018-10-09 08:59:28'),
(27, 1, 120000, 34, 11, '2018-10-09 09:03:07', '2018-10-09 09:03:07'),
(29, 1, 120000, 36, 11, '2018-10-09 09:04:33', '2018-10-09 09:04:33'),
(32, 1, 120000, 39, 11, '2018-10-09 09:05:39', '2018-10-09 09:05:39'),
(33, 1, 120000, 40, 11, '2018-10-09 09:05:57', '2018-10-09 09:05:57'),
(34, 1, 12222, 49, 9, '2018-10-09 09:24:44', '2018-10-09 09:24:44'),
(35, 1, 12222, 50, 9, '2018-10-09 09:26:23', '2018-10-09 09:26:23'),
(36, 1, 12222, 51, 9, '2018-10-09 09:28:24', '2018-10-09 09:28:24'),
(37, 1, 12222, 52, 9, '2018-10-09 09:31:12', '2018-10-09 09:31:12'),
(38, 1, 12222, 53, 9, '2018-10-09 09:32:06', '2018-10-09 09:32:06'),
(39, 1, 120000, 54, 11, '2018-10-09 09:36:02', '2018-10-09 09:36:02'),
(40, 1, 12, 54, 10, '2018-10-09 09:36:02', '2018-10-09 09:36:02'),
(41, 3, 120000, 55, 11, '2018-10-09 09:37:21', '2018-10-09 09:37:21'),
(42, 3, 12, 55, 10, '2018-10-09 09:37:21', '2018-10-09 09:37:21'),
(43, 1, 12, 56, 10, '2018-10-09 09:39:24', '2018-10-09 09:39:24'),
(44, 1, 12, 57, 10, '2018-10-09 09:39:51', '2018-10-09 09:39:51'),
(45, 1, 12, 58, 10, '2018-10-09 09:40:27', '2018-10-09 09:40:27'),
(46, 35, 120000, 59, 13, '2018-10-14 10:15:55', '2018-10-14 10:15:55'),
(47, 1, 150835, 60, 28, '2018-10-14 10:18:51', '2018-10-14 10:18:51'),
(48, 1, 15000, 61, 50, '2018-10-16 03:02:33', '2018-10-16 03:02:33'),
(49, 1, 67000, 61, 48, '2018-10-16 03:02:33', '2018-10-16 03:02:33'),
(50, 1, 56000, 61, 45, '2018-10-16 03:02:33', '2018-10-16 03:02:33'),
(51, 1, 22000, 62, 46, '2018-10-17 04:17:58', '2018-10-17 04:17:58'),
(52, 2, 15000, 62, 50, '2018-10-17 04:17:58', '2018-10-17 04:17:58'),
(53, 1, 7760, 62, 51, '2018-10-17 04:17:58', '2018-10-17 04:17:58'),
(54, 1, 61000, 63, 49, '2018-10-17 04:18:52', '2018-10-17 04:18:52'),
(55, 1, 67000, 63, 48, '2018-10-17 04:18:52', '2018-10-17 04:18:52'),
(56, 1, 22000, 63, 46, '2018-10-17 04:18:53', '2018-10-17 04:18:53'),
(57, 1, 56000, 63, 45, '2018-10-17 04:18:53', '2018-10-17 04:18:53'),
(58, 1, 67000, 64, 48, '2018-10-17 04:25:11', '2018-10-17 04:25:11'),
(59, 1, 34000, 64, 44, '2018-10-17 04:25:11', '2018-10-17 04:25:11'),
(60, 1, 67000, 64, 47, '2018-10-17 04:25:11', '2018-10-17 04:25:11'),
(61, 1, 15000, 65, 50, '2018-10-19 08:43:52', '2018-10-19 08:43:52'),
(62, 1, 7760, 65, 51, '2018-10-19 08:43:52', '2018-10-19 08:43:52'),
(63, 1, 61000, 65, 49, '2018-10-19 08:43:52', '2018-10-19 08:43:52'),
(64, 6, 15000, 66, 50, '2018-10-24 03:21:47', '2018-10-24 03:21:47');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `view` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `listCategory_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`id`, `name`, `image`, `price`, `status`, `view`, `description`, `listCategory_id`, `created_at`, `updated_at`) VALUES
(5, 'Rau Mồng Tơi (500g)', '1539573847-9.png', 6000, 1, 24, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 17, '2018-09-26 08:27:38', '2018-10-19 03:19:06'),
(8, 'Súp nơ (1kg)', '1539574003-12.png', 24000, 1, 28, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 17, '2018-09-27 06:51:23', '2018-10-19 04:43:04'),
(9, 'Cà Tìm (1kg)', '1539574081-31.png', 30000, 1, 0, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 17, '2018-10-02 02:18:56', '2018-10-15 03:28:01'),
(10, 'Nấm Hương (500g)', '1539574118-35.png', 24000, 1, 160, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 17, '2018-10-02 06:39:25', '2018-10-23 02:59:18'),
(11, 'hành tây (1kg)', '1539574183-33.png', 15000, 1, 112, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 17, '2018-10-03 10:18:57', '2018-10-19 09:45:28'),
(12, 'Xoài tượng chín(1kg)', '1539574249-10.png', 20000, 1, 51, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 18, '2018-10-04 09:20:32', '2018-10-15 03:30:49'),
(13, 'Táo Tàu (1kg)', '1539574311-11.png', 18000, 1, 43, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 18, '2018-10-11 03:56:43', '2018-10-19 03:42:29'),
(14, 'Chuối tây chín (1kg)', '1539574348-29.png', 23000, 1, 20, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 18, '2018-10-11 07:48:54', '2018-10-15 03:32:28'),
(15, 'Cam Sành Ngọt(1kg)', '1539574398-32.png', 30000, 1, 2, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 18, '2018-10-11 07:52:37', '2018-10-15 03:33:18'),
(24, 'Dưa mỹ (1kg)', '1539574440-34.png', 120000, 1, 3, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 18, '2018-10-12 09:23:04', '2018-10-15 03:34:00'),
(27, 'Dâu Tây', '1539574482-36.png', 150000, 1, 1, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 18, '2018-10-13 02:58:26', '2018-10-15 03:34:42'),
(28, 'Nước xả vãi comfor (2.5 l)', '1539574597-1.png', 210000, 1, 9, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 15, '2018-10-13 13:53:42', '2018-10-15 10:07:17'),
(29, 'nước rửa chén sunlight (1.5l)', '1539574658-17.png', 40000, 1, 1, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 15, '2018-10-14 09:08:35', '2018-10-15 03:37:38'),
(31, 'vim rửa bồn cầu(1.5l)', '1539574695-20.png', 34000, 1, 1, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 15, '2018-10-15 02:14:34', '2018-10-15 03:38:15'),
(32, 'Nồi Cơm ĐIện', '1539574751-22.png', 250000, 1, 0, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 16, '2018-10-15 03:39:11', '2018-10-15 03:39:11'),
(33, 'nồi đun cháo', '1539574838-23.png', 75000, 1, 0, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 16, '2018-10-15 03:40:38', '2018-10-15 03:40:38'),
(34, 'nồi dun canh', '1539574892-24.png', 160000, 1, 1, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 16, '2018-10-15 03:41:33', '2018-10-19 03:38:25'),
(35, 'nước cam 1', '1539574964-49.png', 8000, 1, 0, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 2, '2018-10-15 03:42:44', '2018-10-15 03:42:44'),
(36, 'nước cam 2', '1539575005-52.png', 10000, 1, 1, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 2, '2018-10-15 03:43:25', '2018-10-15 07:40:25'),
(37, 'nước cam 3', '1539575087-56.png', 12000, 1, 1, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 2, '2018-10-15 03:44:47', '2018-10-16 10:45:32'),
(38, 'coca-cola (chai 1,5l)', '1539575155-2.png', 120000, 1, 0, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 3, '2018-10-15 03:45:55', '2018-10-15 03:45:55'),
(39, 'sprite (chai lơn 1,5 l)', '1539575284-16.png', 15000, 1, 0, 'The new, more vibrant logo stood out more on packaging, and featured a blue-to-green gradient with silver \"splashes\" and subtle white \"bubbles\" in the background. The product name, \"Sprite\" had a blue backdrop shadow on the logo. The words; \"Great Lymon Taste!\" which had been present on the previous logo,', 3, '2018-10-15 03:48:04', '2018-10-15 03:48:04'),
(40, 'sữa tươi vinamilk (250ml)', '1539575831-14.png', 7000, 1, 0, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 5, '2018-10-15 03:57:11', '2018-10-15 03:57:11'),
(41, 'sữa cô gái hà lan 250ml', '1539575868-51.png', 6000, 1, 2, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 5, '2018-10-15 03:57:48', '2018-10-15 03:58:18'),
(42, 'thức ăn mèo 1(1kg)', '1539575968-27.png', 35000, 1, 0, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 12, '2018-10-15 03:59:28', '2018-10-15 03:59:28'),
(43, 'thức ăn mèo 2  (1kg)', '1539576007-57.png', 42000, 1, 1, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 12, '2018-10-15 04:00:07', '2018-10-19 04:43:22'),
(44, 'thức ăn chó 1 (1kg)', '1539576103-25.png', 34000, 1, 0, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 14, '2018-10-15 04:01:43', '2018-10-15 04:01:43'),
(45, 'thức ăn chó 2 (1kg)', '1539576155-26.png', 56000, 1, 1, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 14, '2018-10-15 04:02:35', '2018-10-16 01:47:58'),
(46, 'đồ ăn cho người 1', '1539576201-64.png', 22000, 1, 5, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 13, '2018-10-15 04:03:21', '2018-10-19 06:48:09'),
(47, 'thức ăn cho người 2', '1539576246-73.png', 67000, 1, 1, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 13, '2018-10-15 04:04:06', '2018-10-19 07:23:55'),
(48, 'bột ngọt ajinomoto(1.5kg)', '1539576364-4.png', 67000, 1, 1, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 11, '2018-10-15 04:06:04', '2018-10-16 09:27:52'),
(49, 'bột ngọt aji ngon(kg)', '1539576425-6.png', 61000, 1, 34, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 11, '2018-10-15 04:07:05', '2018-10-19 02:04:22'),
(50, 'bánh trứng sữa', '1539576467-39.png', 15000, 1, 53, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 9, '2018-10-15 04:07:47', '2018-10-22 09:24:30'),
(51, 'bành bơ', '1539576505-43.png', 8000, 1, 21, 'Cao 30–50 cm, thân nhẵn, phía trên phân nhánh. Lá ở gốc có cuống dài, có 1 đến 3 lá chét, lá chét hình hơi tròn, xẻ thành 3 thuỳ có khía răng to và tròn; những lá phía trên có lá chét chia thành những thùy hình sợi nhỏ và nhọn', 9, '2018-10-15 04:08:25', '2018-10-23 10:24:48'),
(52, 'rau', '1539937648-31.png', 120000, 1, 2, 'ấdfghdfg', 3, '2018-10-19 08:27:28', '2018-10-22 09:46:26');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sale`
--

CREATE TABLE `sale` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `dis_count` int(11) NOT NULL,
  `start_at` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_at` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `sale`
--

INSERT INTO `sale` (`id`, `product_id`, `dis_count`, `start_at`, `end_at`, `created_at`, `updated_at`) VALUES
(1, 22, 1, '2018-10-12', '2018-10-14', '2018-10-12 09:07:39', '2018-10-12 09:07:39'),
(6, 27, 3, '2018-10-01', '2018-10-02', '2018-10-13 02:58:26', '2018-10-13 05:42:09'),
(9, 28, 3, '2018-10-12', '2018-10-28', '2018-10-13 14:17:26', '2018-10-14 08:09:24'),
(10, 29, 10, '2018-10-04', '2018-10-26', '2018-10-14 09:08:35', '2018-10-14 09:08:35'),
(12, 31, 13, '2018-10-14', '2018-10-28', '2018-10-15 02:14:34', '2018-10-15 02:14:34'),
(13, 9, 5, '2018-10-13', '2018-10-28', '2018-10-15 03:27:39', '2018-10-15 03:27:39'),
(14, 11, 11, '2018-10-13', '2018-10-28', '2018-10-15 03:29:43', '2018-10-15 03:29:43'),
(15, 13, 4, '2018-10-20', '2018-10-21', '2018-10-15 03:31:39', '2018-10-16 03:12:00'),
(16, 15, 7, '2018-10-11', '2018-10-26', '2018-10-15 03:33:04', '2018-10-15 03:33:04'),
(17, 24, 2, '2018-10-11', '2018-10-25', '2018-10-15 03:34:00', '2018-10-15 03:34:00'),
(18, 32, 4, '2018-10-11', '2018-10-28', '2018-10-15 03:39:11', '2018-10-15 03:39:11'),
(19, 47, 2, '2018-10-21', '2018-10-25', '2018-10-15 04:04:06', '2018-10-15 04:04:06'),
(20, 51, 3, '2018-10-14', '2018-10-25', '2018-10-15 04:08:25', '2018-10-15 04:08:25'),
(21, 5, 1, '2018-10-19', '2018-10-20', '2018-10-18 10:20:24', '2018-10-19 01:41:20'),
(22, 52, 3, '2018-10-18', '2018-10-27', '2018-10-19 08:27:28', '2018-10-19 08:27:28');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `slide`
--

CREATE TABLE `slide` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `slide`
--

INSERT INTO `slide` (`id`, `image`, `status`, `created_at`, `updated_at`) VALUES
(3, '1538367973-2.jpg', 1, '2018-10-01 04:26:13', '2018-10-01 06:38:53'),
(4, '1538562868-1.jpg', 1, '2018-10-01 04:26:23', '2018-10-03 10:34:28'),
(5, '1538563725-2.jpg', 1, '2018-10-03 10:48:45', '2018-10-03 10:52:24');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` int(11) NOT NULL,
  `birthday` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lever` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `avatar`, `address`, `gender`, `birthday`, `lever`, `phone`, `remember_token`, `created_at`, `updated_at`) VALUES
(15, 'tung', 'tungthanhnp@gmail.com', '$2y$10$afR2rseQkMdkez5/tCayPeIjz45xc/es7B5kRXVH7.Q8FIbKiQLEu', '1538364774-6.png', 'nam phu-nam phong', 0, '1993-10-05', '1', '0989251692', 'No2rf22cvqFDoStxW9Bov8mUtYWEqobWIMpDSCdfUngVXqBTCwahStPTZYq0', '2018-10-01 02:44:08', '2018-10-17 09:51:03'),
(17, 'new user', 'tungthadddddhnp@gmail.com', '$2y$10$F4C9U95Tc73cn066I4/r9OUkFSeOsxGSHiD7OOGwhvm3rR9ZSdUqC', '1539657684-8.jpg', 'nam phu-nam phong', 0, '2018-10-18', '0', '0989251690', 'fOGMSSNMdSv6MXQ2Y05iS0cH9wD2I7TjcIQilenB', '2018-10-16 02:41:24', '2018-10-16 08:17:32'),
(18, 'Nguyễn Văn A', 'tungthanhnp1@gmail.com', '$2y$10$5rq1DyXuPVLQDewGcZ54feo9sc0wFhywhMROqug6uxf7poqGPPz.S', '1539931816-30.png', 'nam phu-nam phong', 0, '1993-10-05', '0', '01649671393', 'NRoRFUwsYo0yois86k6zh7JyHQ642B8cLWNUTwNI', '2018-10-19 06:50:16', '2018-10-19 06:50:16'),
(19, 'Nguyễn Văn C', 'tungthanhnp2@gmail.com', '$2y$10$7YTBubvzDtUrJ9HiIb9mK.PAUsAb4UQnElI2NJf..7W5upmmF8rPC', '1539931861-33.png', 'nam phu-nam phong', 0, '1993-10-05', '0', '0989251692', 'NRoRFUwsYo0yois86k6zh7JyHQ642B8cLWNUTwNI', '2018-10-19 06:51:01', '2018-10-19 06:51:01'),
(20, 'Nguyễn Văn E', 'tungthanhnp3@gmail.com', '$2y$10$cQtsTk/dpep3qqeHAVUTM.Ns5ru5lbrKIW/NWhXGgehR98gq.jT7e', '1539932274-12.png', 'nam phu-nam phong', 0, '1993-10-05', '0', '0989251692', 'NRoRFUwsYo0yois86k6zh7JyHQ642B8cLWNUTwNI', '2018-10-19 06:57:54', '2018-10-19 06:57:54'),
(21, 'nguyen văn d', 'tungthanhnp7@gmail.com', '$2y$10$RCxhXZ8rD93DZCHwVZ8UdeBzUnpYPTOGPjxIPirnYfOUpRXFwdzZa', '1539937063-33.png', 'nam phu-nam phong', 0, '0001-01-01', '0', '01649371393', 'NRoRFUwsYo0yois86k6zh7JyHQ642B8cLWNUTwNI', '2018-10-19 08:17:43', '2018-10-19 08:17:43');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comment_user_id_foreign` (`user_id`),
  ADD KEY `comment_product_id_foreign` (`product_id`);

--
-- Chỉ mục cho bảng `list_category`
--
ALTER TABLE `list_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `list_category_category_id_foreign` (`category_id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_user_id_foreign` (`user_id`);

--
-- Chỉ mục cho bảng `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_detail_order_id_foreign` (`order_id`),
  ADD KEY `order_detail_product_id_foreign` (`product_id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_listcategory_id_foreign` (`listCategory_id`);

--
-- Chỉ mục cho bảng `sale`
--
ALTER TABLE `sale`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT cho bảng `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=196;

--
-- AUTO_INCREMENT cho bảng `list_category`
--
ALTER TABLE `list_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `order`
--
ALTER TABLE `order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT cho bảng `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT cho bảng `sale`
--
ALTER TABLE `sale`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT cho bảng `slide`
--
ALTER TABLE `slide`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `comment_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Các ràng buộc cho bảng `list_category`
--
ALTER TABLE `list_category`
  ADD CONSTRAINT `list_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

--
-- Các ràng buộc cho bảng `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Các ràng buộc cho bảng `order_detail`
--
ALTER TABLE `order_detail`
  ADD CONSTRAINT `order_detail_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`),
  ADD CONSTRAINT `order_detail_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

--
-- Các ràng buộc cho bảng `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_listcategory_id_foreign` FOREIGN KEY (`listCategory_id`) REFERENCES `list_category` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
